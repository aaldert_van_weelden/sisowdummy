<?php
return [

		'environment' => 'production',

		'app_url' => 'https://sandbox.elearning-wizard.nl',

		'app_root' => '',

		'storage' => 'storage/store.txt',

        'config' => 'storage/config.txt',

        'index_response' => 'HOMESERVER SISOW API sandbox',

		/*
		 |--------------------------------------------------------------------------
		 | Root logger configuration
		 |--------------------------------------------------------------------------
		 |
		 */

		//enable query logging
		'log-query' => true,

		//logging levels to write to debug.log
		//values are TRACE, VERBOSE,DEBUG,INFO,NOTICE,WARN,ERROR
		'log-level' => 'TRACE',

		//logging filtering, add exclusion TAG comma-separated format, case insensitive



		//logging filtering, add exclusion TAG comma-separated format, case insensitive
		'log-exclude' => '',

		//filtering the logging message and tag,  comma-separated format, case insensitive
		//'log-include' => 'ManagementUsercourseService',


		'remote_adress' => '192.168.1.15',

		'log_dir' => '/home/aaldert/sisow.vwit.nl/',
		'DEFAULT_LOGFILE_NAME' => 'sisow-info-debug.log',
		'ERROR_LOGFILE_NAME' => 'sisow-error-warn.log',
		'QUERY_LOGFILE_NAME' => 'sisow-query.log'

];

