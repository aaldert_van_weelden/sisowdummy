<?php

use App\Application;

if(!defined("DOC_ROOT")){
	define("DOC_ROOT",dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR);
}

error_reporting(E_ERROR);

require_once DOC_ROOT.'bootstrap/autoload.php';
require_once DOC_ROOT.'app/app.php';

$app = Application::create();

$app->checkIP();

$app->setActiveMerchant();

/**
 *
 * @return \App\Application
 */
function app(): Application
{
    return Application::getInstance();
}


/**
 *
 * @param string $key
 * @param string| int $default
 * @return string|int|bool|null
 */
function env($key, $default = null)
{
    return app()->getEnv($key, $default);

}

try
{
    $app->init();
}
catch (Exception $e)
{
    header('Content-Type: application/json');
    echo json_encode(["error"=>$e->getMessage()]);
    exit;
}


?>


