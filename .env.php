<?php
return [

		'environment' => 'local',

		'app_url' => 'http://sisowdummy.localhost',

		'app_root' => '',

		'storage' => 'storage/store.txt',

        'config' => 'storage/config.txt',

        'index_response' => 'LOCAL SISOW API sandbox',


		/*
		 |--------------------------------------------------------------------------
		 | Root logger configuration
		 |--------------------------------------------------------------------------
		 |
		 */

		//enable query logging
		'log-query' => true,

		//logging levels to write to debug.log
		//values are TRACE, VERBOSE,DEBUG,INFO,NOTICE,WARN,ERROR
		'log-level' => 'TRACE',

		//logging filtering, add exclusion TAG comma-separated format, case insensitive



		//logging filtering, add exclusion TAG comma-separated format, case insensitive
		'log-exclude' => '',

		//filtering the logging message and tag,  comma-separated format, case insensitive
		//'log-include' => 'ManagementUsercourseService',


		'remote_adress' => '127.0.0.1',

		'log_dir' => 'C:/Users/Aaldert/phplogs/',
		'DEFAULT_LOGFILE_NAME' => 'sisowdummy-info-debug.log',
		'ERROR_LOGFILE_NAME' => 'sisowdummy-error-warn.log',
		'QUERY_LOGFILE_NAME' => 'sisowdummy-query.log'

];

