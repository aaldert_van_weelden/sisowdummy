<?php namespace App;

use Utils\Logger\LoggerInstance;
use Utils\Store;
use XHR\HTTPcodes;
use Utils\Config;

/**
 * This is the global application singleton class
 *
 * @author Aaldert van Weelden
 *
 */
class Application{

	const TAG = 'Application';

	public static $instance=null;

	private $log;

	/**
	 *
	 * @var array
	 */
	private  $environmentvars;

	/**
	 * The persisted storage
	 * @var Store
	 */
	public $store;
	/**
	 *
	 * @var Config
	 */
	public $config;
	public $clientIP;
	public $activeMerchant;


	/**
	 * Private constructor
	 * Add the configs
	 */
	private function __construct()
	{
		$this->environmentvars = include_once DOC_ROOT.'.env.php';
		$_ENV = array_merge($_ENV, $this->environmentvars);

		$this->log = new LoggerInstance(self::TAG);
		$this->store = new Store($this->environmentvars['storage']);
		$this->config = new Config($this->environmentvars['config']);
	}

	/**
	 * Create and return the application singleton
	 */
	public static function create() {
		if(!isset(self::$instance)) {
			self::$instance = new Application();
		}
		return self::$instance;
	}

	/**
	 * Retrieve the application container
	 * @return \App\Application
	 */
	public static function getInstance()
	{
	    if(! self::$instance) self::$instance = new self();
	    return self::$instance;
	}

	/**
	 * Initialize the application
	 */
	public function init(){
		$this->log->info('init: Initializing the application ');



		$routes = new Routes();
		$routes->add();
		$routes->config();
		if($routes->getCurrentRoute() === false)
		{
		    $error = "Invalid request path";
		    $this->log->error(__FUNCTION__.": ".$error." ".$_SERVER['REQUEST_URI']);
		    throw new \BadRequestException($error);
		}
		//var_dump($routes->getCurrentRoute());
		$this->log->trace('REQUEST_URI: '.$_SERVER['REQUEST_URI']);
		$this->log->trace('REQUEST_IP: '.$_SERVER['REMOTE_ADDR']);

	}

	/**
	 *
	 * @param string $key
	 * @return NULL|mixed
	 */
	public function getEnv(string $key, $default = null)
	{
	    if ( ! array_key_exists($key, $this->environmentvars)) return $default;

	    if(empty($this->environmentvars[$key])) return null;
	    if(preg_match('/^[0-9]*$/', $this->environmentvars[$key]) === 1) return intval($this->environmentvars[$key]);
	    if(preg_match('/^([0-9]*).([0-9]*)$/', $this->environmentvars[$key]) === 1) return floatval($this->environmentvars[$key]);
	    return $this->environmentvars[$key];
	}

	public function checkIP(){
	    $whitelist = parse_ini_file(realpath(__DIR__).DIRECTORY_SEPARATOR."config".DIRECTORY_SEPARATOR."whitelist.ini");
		$this->clientIP = $_SERVER['REMOTE_ADDR'];

		//var_dump($this->clientIP);var_dump($whitelist);die;

	    if(! in_array($this->clientIP, $whitelist)){
			$this->log->trace("IP [$this->clientIP] is not allowed");
	        $response = new \XHR\SimpleResponse(Application::class);
	        $response->setData(["error" => "Not authorized"]);
	        $response->sendJSON(false, HTTPcodes::HTTP_FORBIDDEN);
	    }
		$this->log->trace("IP [$this->clientIP] is whitelisted");
	}

	public function setActiveMerchant(){
	    $this->activeMerchant = $this->config->read('merchantkey');
	}
}