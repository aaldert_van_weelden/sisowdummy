<?php

namespace App\Controller;

use Utils\Logger\Logger;
use Utils\ObjectToXML;
use Utils\Util;
use Utils\REST_HTTPS_Request as RestRequest;
use App\Application;
use Utils\DateTimeUtil;

class IndexController {
	const TAG = 'IndexController';

	/**
	 *
	 */
	public static function index(){

	    self::jsonResponse([
	        'message'=> env('index_response'),
	    ]);
	}

	public static function setConfig(){

	    $data = file_get_contents('php://input');

	    Application::getInstance()->config->writeAll($data);

	    self::jsonResponse([
	        'message' => 'config array written'

	    ]);
	}

	public static function getConfig(){

	    self::jsonResponse(Application::getInstance()->config->readAll());
	}



	public static function healthCheck(){

	    self::jsonResponse( [
	        'message' => 'OK'
	    ]);
	}


	public static function getAllIssuers() {
		Logger::log ()->debug ( 'called getAllIssuers' );
		// Logger::log()->dump($_REQUEST);

		$data = ( object ) [
				'directory' => [
						'issuer' => [
								'issuerid' => 666,
								'issuername' => 'VWIT Bank'
						]
				]
		];

		$parser = new ObjectToXML ( $data );

		Logger::log ()->dump ( $parser->__toString () );

		self::htmlResponse($parser->__toString ());
	}
	public static function handleTransactionRequest() {
		Logger::log ()->debug ( 'called handleTransactionRequest' );

		// Logger::log()->dump($_GET);

		/*
		 * ======== PARAMS ================================
		 *
		 * 'shopid' =>
		 * string(1) "0"
		 * 'merchantid' =>
		 * string(10) "2537802310"
		 * 'payment' =>
		 * string(1) "0"
		 * 'purchaseid' =>
		 * string(16) "1002967401592017"
		 * 'amount' =>
		 * string(7) "2000000"
		 * 'entrancecode' =>
		 * string(32) "b602f02815a0337caf5621d39184c464"
		 * 'description' =>
		 * string(13) "JAARABONNEMENT"
		 * 'issuerid' =>
		 * string(2) "99"
		 * 'returnurl' =>
		 * string(52) "http://laravel-academy.localhost/transaction/success"
		 * 'cancelurl' =>
		 * string(52) "http://laravel-academy.localhost/transaction/failure"
		 * 'notifyurl' =>
		 * string(51) "http://laravel-academy.localhost/transaction/notify"
		 * 'sha1' =>
		 * string(40) "529f713727a04a8437a9333ada77fcf8a5746e8b"
		 * 'testmode' =>
		 * string(4) "true"
		 */

		// ============ RESPONSE ============================

		/*
		 * <?xml version="1.0" encoding="UTF-8"?>
		 * <transactionresponse xmlns="https://www.sisow.nl/Sisow/REST" version="1.0.0">
		 * <transaction>
		 * <issuerurl>IssuerURL</issuerurl>
		 * <trxid>TransactionID</trxid>
		 * </transaction>
		 * <signature>
		 * <sha1>SHA1 trxid + merchantid + merchantkey</sha1>
		 * </signature>
		 * </transactionresponse>
		 */

		$params = ( object ) $_GET;

		Application::getInstance ()->store->write ( 'transaction_params', $params );

		$trxid = 'DUMMY_00' . Util::create_number ( 8 );

		$issuerURL = $_ENV['app_url'].'/confirm';
		$merchantKey = Application::getInstance ()->activeMerchant;

		Logger::log ()->debug ( 'handleTransactionRequest: trxid=' . $trxid . ' , merchantid=' . $params->merchantid . '  ,  merchantKey=' . $merchantKey );

		$sha1 = sha1 ( $trxid . $issuerURL . $params->merchantid . $merchantKey );

		Application::getInstance ()->store->write ( 'trxid', $trxid );
		Application::getInstance ()->store->write ( 'issuerURL', $issuerURL );

		$data = ( object ) [
				'transaction' => [
						'issuerurl' => $issuerURL,
						'trxid' => $trxid
				],
				'signature' => [
						'sha1' => $sha1
				]
		];

		$parser = new ObjectToXML ( $data );

		Logger::log ()->dump ( $parser->__toString () );

		self::htmlResponse($parser->__toString ());
	}
	public static function confirm() {
		Logger::log ()->debug ( 'called confirm' );

		header ( "Content-type: text/html; charset=utf-8" );

		$trxid = Application::getInstance ()->store->read ( 'trxid' );
		$ec = Application::getInstance ()->store->read ( 'transaction_params' )->entrancecode;
		$status = 'Open';
		$merchantKey = Application::getInstance ()->activeMerchant;
		$merchantId = Application::getInstance ()->store->read ( 'transaction_params' )->merchantid;

		// recepy; trxid/ec/status/merchantid/merchantkey
		$sha1 = sha1 ( $trxid . $ec . $status . $merchantId . $merchantKey );

		Logger::log ()->debug ( 'notification: trxid=' . $trxid . ' ,ec=' . $ec . ', status=' . $status . ', merchantid=' . $merchantId . '  ,  merchantKey=' . $merchantKey );


		Logger::log ()->debug ( 'calling the issuer URL for trxid[' . $trxid . ']' );

		//---------------------------------------------------------------------------------------

		//show dummy issuer payment form here
		$w = '';
		$w.= '
		<html>
			<div style="width:800px;margin:20px auto 0px auto;">
			<br>
			<h2 style="margin-left:77px;">SISOW DUMMY Issuer Environment</h2>
			<br><br>
		<h4>Please pay!</h4>
			<table>
				<tr>
				<th align="left" width="200px">property</th>
				<th align="left" width="600px">value</th>
				</tr>
				<tr><td>trxid</td><td>'.$trxid.'</td></tr>
				<tr><td>issuerURL</td><td>'.Application::getInstance ()->store->read('issuerURL').'</td></tr>
			';

			foreach (Application::getInstance ()->store->read('transaction_params') as $key => $value){
				$w.= '<tr><td>'.$key.'</td><td>'.$value.'</td></tr>';
			}

			$w.='
			</table>
			<div style="margin-top:50px;">

			<a style="display:inline-block;padding:15px;background-color:#e0e0e0;border-radius:5px"
				href = "/authorize/">
						Authorize payment and return to client...
			</a>

			</div>


	       <img style="display:inline-block;float:left;margin-left:10px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAfQAAAEJCAIAAADtuEIaAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAR5pJREFUeNrsvWd8Hcd1939mZsut6L0SAHvvolglqrlFsiQ7lh3bT4qTv1MeO8VOnthxie0ktiz32Jarii1Z3WqkGiWx9wISbACI3svF7XV3Z/4vQEqkCBLYxV4AJM73oxciCeydO7v7m5kzZ36HCCEAQRAEub6g2AUIgiAo7giCIAiKO4IgCILijiAIgqC4IwiCICjuCIIgKO4IgiAIijuCIAiC4o4gCIKguCMIgiAo7giCICjuCIIgCIo7giAIguKOIAiCoLgjCIIgKO4IgiAIijuCIAiKO4IgCILijiAIgqC4IwiCICjuCIIgCIo7giAIijuCIAiC4o4gCIKguCMIgiAo7giCIAiKO4IgCILijiAIguKOIAiCoLgjCIIgKO4IgiAIijuCIAiC4o4gCILijiAIgqC4IwiCICjuCIIgCIo7giAIguKOIAiCoLgjCIKguCMIgiAo7giCIAiKO4IgCILijiAIgqC4IwiCoLgjCIIgKO4IgiAIijuCIAiC4o4gCIKguCMIgiAo7giCICjuCIIgCIo7giAIguKOIAiCoLgjCIIgKO4IgiDTGQm7AEEQUxg8mjD6NB7gECegyDRDZQUyzcaeQXFHEOQa1HQRH0rs7Y+/Gkye0LjfEHEADkAoUSWa4ZaqC5y35btuUWg+9tVUgAghsBcQBLk6/fHXWkO/DGunAICCAoQRIAAEQACAAIMLTQjdIZWUeT5e7vkkI27sNBR3BEGmLjoP1Qe+2RN7iYJEiXr1Hxag6zyaqSydn/NfHnkO9h6KO4IgU5EU99UNft6fPCBRLwAZ428ZIqrQ/EW5P8xSV2AforgjCDLl5uy1g58NJI9I1GP2d7lISDRrad6DGcoi7MlJAVMhEQQZmYbA//iTBy0oOwBQ4tT40OmhL2k8iD2J4o4gyFRhIP5mT+wFmWZYvYBgxBXRzraGfoGdieKOIMiUQABvC/8aAMYeZx8RRt3d0Wei2jnsUhR3BEEmH19iVzB1ghHHOK9DgGk80Bt7GbsUxR1BkCkg7vEdQujjnLaflxiiDiTe4iKJvYrijiDIZCKEHkzVUiLbIzFEjuvtYa0eOxbFHUGQySRh9CaMbkLs8iYhhkjEtCbsWBR3BEEmE40HDJEkNoqD4Ck+hB2L4o4gyGQiQBPCsPeaXCSwY1HcEQSZTAhIhNisDIQo2LETDFr+XgMkUoYvlEwkOWPE65KyPAqj5Nr9OrohAuFUOKYJAK9LyslQr+mvcx2KAs2gROUiRcCm+0KoYp/buy6iCb1r+OCrTDOcUikjHrxrKO7Xlgjy1w71PPp604HTPl8ooXNBAZyqVJLnvH1lyadur15Sc42VRzjTFvzN1nOvHuzu9sUSKQMAVJlVFro/vL7iTzfnzK/IBpgSPrG9Q7FTrf6m7vBAIKkZhIBQFVKS65xVlrFgRnaGa0pMQgORxKF6X3170BfShCAOBapLPCtm59aUZI7zyg5W5KCFUb3Fpum2YKC45KrxXyiqNXdGH/PFd6W4bzi3khJVobl5zpvKPJ9wSTNQNC4ZUtE4bGrS2BX+6wf27djbCQJAoUAvLJOFAIODxolL/sInFv7PXy+7Vqa9Dzx1+ssPHk2FkqAwkMiFHGoBOocUyJnGX3/U/9VPLSx03w7AJquRe072/vCZM68f7g35E6AL4Be9HZSAQkuL3ffdXPn5e+eXF0zabFHTjR89d+p7T53t7Y6CLuCdV5gRxSt/aG3pf/750oVVOeMahv3/0RV9WrJjRsyF5pRKVxc+y4hrPNfpiPy+KfhDnYcocZDzbvLD19e5SMg0sybzn8o8n0DpQHGf0rx1rPdjX90x2BcBt3KVlwYiqb/62IJf/+uNU/8bffHBow/8phacDNgVgrkGgThfta7uF/+Wtaz4XwHkCW6hL5T4x58e/P2WZkhxcEjARhoyBYDOIaF78x3f/uyyv7tr/mRM2JMf+8b219/sAIcMymWdaQiIa2qG/N9/u+yfP2LdjrEv9urJoX8apxxfiKJEyj2fnpP15fFcpCn4g5bQzxl1kisEGwToBo9Xej8zK+tfUUDOj/Vf//rXsRemFIcbfB/6wptD/ji4ripwhIDCjh3vryj1LpuVM5W/0Y+ePfPVnxwCl3xFZQcACqCQ7nMVxzoab1nbmq2umsgWtvSG3v/FN97Y3gmqDCqDKy2GCAAjoLJU3Ni6vT3K9dtXlk5kOw3OP/a17a9s64AMdeThhxJQmKHB6zvaU4zfsrzE2gc5aflgcnvK6BtntrsAzog8K+PfHVKB5Yt0R59tDHyHUTe58pKOAKVEGUrupYRlq6tRRlDcpxyxpPGRL7/d3BYE5ximroQAwMlm/yfvqHGpU3T75FRr4ONf3akRuJqyv4NqdDWVE/eh9y1bADBBOwq9Q7EPfOGNutN+8Chj3UFkBCS690APVWHT0uIJ68xvP1b74ONnIWO0UDglILHdB7pnV2csqrYy8FMiUVD6E2/Q8YXdDREpcn243P1xcf5pNR+D4v5TQ1/gIklHH2YIJXIwVZvrWK+yAhQTTIWcWvz6pYbDtX2jzNkvRmHNLYEfPX1myn6j/33mbDSYAGnMT5oqHn2+5nDn1olpnmYYn/zmjpOn/eA2GQiiBFzy135x/Im3J+jsZWNX4Lu/Pw1OaazNk9iXHzzmj5g2dRFCcC6KnHfnO27VRdhyg7lIOFl5lffvBYAQ3OLQG3sprneOcYwhwHQe6Yo+g0qC4j610HT+qxcbQDG5nahKv93SOBicisZM/f7EczvbwGFmVSEb/r7iZ7Y3APROQAvv/0Pdm7u6wWMpxE8JEPJPPzzUNRidkGHyTGDQzDCpsNa20DNvN1sLpxBC5mR+xSPNMUTMgoMYFxoljjlZ/+lkJVwYhFjc9h9M7DQVGqLE4U/uM0QU9QTF3ewjK5KakaaLHz/nP9kWHGGXbBQ1pF0docdeb56C3fXy3s7+vqgJPTo/AZP21MpD2oF0N69zIPKjJ86YG3suE9Deruh3/1CX7qaG46kXdneabiohW/d1WX3UDQcrWpTzvy6pShchs3N2QqR5Wd/Oc6w3xnHYVefhuN5OwZS4s6QxENc7UaxQ3M1xrNH3wBMn03Txc10hSOpWYpMSfeyNZoNPucSnp99utXIOhvGegcyOwI50N++RVxoHemMgj+8tcEjPbm/3h9N7vP54o6+t1/wwKbFT7cGkppkcEc7fM0MYbqlyae6vc9QNughzMfp1BBg6DzlY6eKcnxU6bzP4uGZCSaNfM/xATK1lqSESKWMQxQrF3RzffbLu58/Wh6JaOi6e1DhY02dVOnR6YNfxvinVV+e6wrvq+kGxMi82DKc/UatxX/qal9KNp95uMx0EG2nl1NkZ2bq/I62duWV/JyQM0yMlBV9Ii8a55c81hOFgJUtyfj4746sqK9BF2BAJAcblYRwuUroIU6KWeT69PO+xXHWNMW6DmhQfMkTc/EFZnuJ+FCsUdxNsP979zI72rrPBZ7emJQaS5VEsphQQgBR/ZnvblOquLXs7okPxkTP2Rpn+kUyvRuWeQOJs+pq3/1T/icaADeIOAAJeOdCd1s7cf9pneto+/IZTK8/UxSFyLgwCcoXnkyvzn5qd+R+ZylJGXFykDBHVRcQQUUMkKEguqarC/dcrcv8wN/MrKst/j7Jbi7kbIiqAm434C8F1Hka9QvuBsaLp/D9+c9QIceD098+d+4uPzrH9I6qLPcwlGYYAC4dOFfbC7o5vfmZZtneqODS9vK9rTOmPI0y8SHFeWJISvsTefNe6dI09+zogZYBsh7irbNuRnoFAPD/LmY6m+kLxM21BK+LOIS9DcTtNf0dCLjneKIAbAmSSXeH+dKnrE0mjN250JYxOnUcoUR2syMHKHaxUpg4BcPmE3fJuKhdJsLKYFYaIo2ShuI+Vh16t33OkH0AGpzjQGDjXEZ5Z7rX3I2pKvRUF7pbOsBVxl2lnV+j1Q90f2zxjKnTX2bbgrhN9oFqUzrkzexllYe1U2oZq46W9XfYoOwBItK83vvtE790bq9LR2qMNvr7+GMjm31aDVxS6Fcn0L44ox8MSD8CcUplLKgO44aJ/AiHEleIw4xD3lNWlVAqmPRiWGRODwcT9fzgFGgUhQIWonnpmh/0xEJdDWlKTDZazcTi8drB7ivTYC7s7kqGklVFKEOpMLZnbaehqXG9P6GkJnh46O3CmNWSbuAOAIV7am64MjdrGIUhxK1vTXFQXWZyCUHolceBcGMal/3FhCOBXUnbL4i6EbmnmDmPZ+0VxRwAAvv90XVNj8LyhlQTgJI9saUgkdds/6P03lILlnBeVvXqwa4okvL+8t8NiTEZn5cX+8hK/biga7w+n0hJ2f/toD8R1sNFyTab7Tw+m9LSkyR5v8oNFezixeGaWtQ8djyiPbZAYywQcna9Q3NNJ7bnBnzxXD0QCLoARkAg42dn6oW377J8mb1hSqGYoYC2pUaI9PZHdUyBn5mRz4GC9z+JepcY2rDqX4YlzTgVoES0t4r7jeL/FseeK4s7OtIWONtifgZfU9EP1PpDMd6YAcEorZudZ/ujxi/t4lB0ABOioPyjuaeRrDx+LDKSAEyAAEgAFEAAG2bLb/mX4vMrMm5cVQdLqBNAQz+5sn/Qe27KvMxW0GJMBVduw8pxh0GFxCafqbW9ee194/+lBe/Jk3hVCgJi+45j9p2qbukLN3VGQzHemwfOyHNUl1neGCCHjUWdK6fiHB5y6o7ini+d3t764sxOGD0AzAuxCXpZLfulAT9+Q/UdX7tlQAZaPI6nszSM9Q6FJjsy8cqDLWt4eaGzezJ45Nb2plAQAlMhh7bRuJO19/2vPDYUDKSs5mleH0d0n+23vzCMNPj2qWRkpdT671JOXOa4EHkIIY8yCRtui7AiKe7qIJfVv/u44xAH4sBPTRR3moF3dwWffbLH9Q+9cX55f7AHd0sETifb0RnbUTmZkpq7Jv+dk/3hiMm5XigsCAASkFO+K6aOtRUyq+/baHjDSMCNU2J6Tgz0+m11Ndp3oB2uHkAy+cZk95oimlNryeICguE8cv9pSf7R2EAgDAsDekzgqQCaPvWa/I2BhjnPTkgJIWY/MPP32ZJ5m2nakR49YmmlyInsTa1c0p7R3BgZqiGgwebWESLPFZrjgbx3ts7iwGOVlIn5f4vi5IXuverI5YHGRweiN821zvqWUjirZw7I+zjg7guKedroGo995/CRwBhyAwbsBmYtiIIdO+46dtf+I/MdvqQbLxfNU9vrh7h7fpB3ieHp7m0UxSknLFnTMmtGnadJFeiFCWu3VZ+2m5ogtPeGGzsh4/WRG1jYAXew4bmfYvd8fa+gKW9lNNYQrQ1lUbbMn/rDE0wsMZ9QM/z/KOor7NcO3flfb0xYBoEAviPtlMzUtajz5hv3T5NtXl8ysyrI4eZeorz+6o7Z3UjrtaIPv4Bmre5WC3LHhtCwbF0/GCUhR7dxVpufEZFBmT11f3Npm79g6/43DvTbuAh5pGPQNWLJw0PiSmVlVxRnp+JbkAhdLPCoGivu1wd5Tfb9++RwQGWBY2a9gXuSWfr/lnM9v87aqxyndubbcemQG4LlJypnZuq/TiKSsSKdBs/JDyxe2J1PypToiJYzOhD545ZiMuc9683AP8LR9f5meagnWdwTsul5tox80S8eXDL5mXh6+yCjuyHtF4+sPH9ODHAQ5XzbzSv2kkq7+0PYjPba34N6bKsAlg7Xy5Qp7+2hv39AkRGa27LeaJ5OU161oLi0K6Dq9dGIuaXwglDp95VmkiQ+JxFO76gZsToK8dDGXCCZttOc8dm7I4iKDwqp5ufgmo7gjl/D4tqY39nQDZQDikvTHK6zE02FFsHZhweq5uRYT3iU6OBDbloYhZ5QYQr3vwJlBK34yAkAyNt3QwPmI/2jbUaa6Zn9LdyQtu6kXDTd76gZsuVA0kTp81mfFI8EQnmz1xgVYRxTFHbmIUCz1X787ASkCYrgO8midpLJX9/e0dUdsb8ndGyqsZ+wJ8dyOiY7MvH6oW8Q0Kw6zujSjcmDFwrb3xGQuzM1ZRKsf6Suarv524PSAFVd0k8umnSf6ogkbjKvOdYba+mJWAu46n1uRMaPIm75vKcDQeVjjAYNHIY1xLgTF3T6+/9TJ06cvFH+hI+2jXjZNDvTFHvljo+0tufemSmemavFAk8J2n+zv9ycmrN+EgOd2tls+u3Tj8uYMT4JzMlKoQw5rZzRjhPxxs7L32sFuMJvRwYW5WyDR5q7oyWYb/M5OtQZ4XLd2fGnZzGxIyyAmhpL7Tw/9+6H++w703XOw7+4D/fcc7v9kQ+B/wtoUqtJOADd40fL3Us62B37w1Jnz3TKc2D6Wh8TFntre9m9/s0S1NcFuVlnGhsWFr+/pGGvB+0slpr8n8sqBrv/zvpqJ6bq6Zn/tuSErMQROmDt52/ozms6u8KJKKd4T1duz2LyLAznDaZBj/xxfMH600W8uCVLji2dn+MNaR19irDNoApDQ958euGF+4Ti7dP/pAYtzYgq3rCi2/RYnjK56/38NJnYI0CnI5MLEJw6dgeThruiTxa67Zmb+i0Qtp+jwhNGT0Hs0HhRgUOIIp85QsOBXLEf1Fl9ipyHeM7kRBGSFZjukUpVd/zErFPdL+ObvakN9SaASEACJwBiVysFOdQUOnhrYsLTQ3vb82W1Vr++2Hl15Zf/Eifvzu9r1cAo85kuFpKRly5tnV/eltCs9jYSLWFRrylLnXRwXMJt+d7LF3z8YN5cznjL+4v0zD5wZfKKj1cQQS+lrB7s//5GF45wj7zs1aKlAh3BmKCvn2pwqE9Pbjg/+f1GtWaIeAMfF/8RAAuIQwDsij0W0xiV5/yvTHJNN1vrjr3RHn4toZ3UevVDGTxAiU6KabSojzoH4tr7Y1hHHfgKSTDOy1FWV3j/PUJZgWGZasO1I1x9ebwN6kY3MmBeBoPPHX7e/9t6tK4tzC9wWrQhU9sbh7q7B2AR0ncHFC7s7LNqjc7rxhkZV1q+WzE5EMHniEuUzn0v+yoEuSJoJuAsAhd68rOS2FSXAzdwCmR5t9PtC48pW6h2KNVnb+9X4oprMmhI7M9x1Hjrp+5eo3ipR75VWSwSoTDMDycMnfV/gwoS7UdLoPT742ZO+L/iTBw2RoERmxMmIkxEXBdnauEhAYsR14TqX/EeJpItwX3zr4f5PtYZ+juJ+/aMb/BsPHxdhA+AyG5kxKan05Nbmrl6bfUVK8lx3ri2zmDPD6NBA7JX9XRPQe4fODB5t8IFi/nEyaGZ+aP3Kc4mUdNXRUw5rJ/gF+RfCSlB178kBc1qp87Ji17wZmctm54JDMjGaMNo3ED/RNC4fgvr2oD+QtBZwv3F+vr0B97bwb4OpWom4Rw8FUO9gYmd7+JExj0ShE77P+RI7JeplxDlaapotEAKSRDyE0HPB77eHH0Jxv8755cv1uw72gHQh2m52DsqI35d8Zbf9SvqJ26tBYZbPPD6/ayJyZl471A0J3UqeTEpaPr+9tDCg6+yqr6OU0DvjWu/FL6gpugcjJ1uC5tYWmrF+Yb4iSXMqMqvLzKyfCEDSGGdVrF11fRYTewisnGNnhnvKGOyJPT8WZb/wKri6ok9qPDCWH24J/SyQPDqOMP04NB4YI87W8K/iRidcj6C4n18Cf+f3dWAwEGb2Ud/zPjvIc7vtV9L1iwpmV2ZarL2nsj0nBzr6o2ntPd0Qz+1os1hNgonbNpwZy3uoiaHwlY8yjcruun7/oOlz/LevKgEAlypvXGTSyk1i22v7xuNDcPjMEFiwajGEK0u5cWG+jffXnzycNHoJGeuWAyVywugeSuwdwxojOBDfxohrsl58QuSUMTAQex3F/brlgSdPtjeFgdDz+6jWesXJtp/qO2G3KaBDYX+6eQZolsLujAbSH5k50TRU1xK0FJNhRUX+5Qvbk6mxCIcR1s8CWDy0u+1It7nMEy4cmcqGxUXDf7pjdYm5p0KhtY1+yz4EwWjySIPPiruZzudWZNSUZNp4fwOpw8JkpwvBh5Kji3tYq08avZRMZloHISyiNcL1CIo7HG8e+vkfG4DK7wZkrAX9GIkPxX/zrP1l4e5cVw5OyaKqEXjlQHrF/bmd7SKWshaT2bCqMScranA6hpeQRi5UZTL7USld33W832wS5OxSb9WFMkZr5he4s1QTZ8ooSUa0Yw0WR/pznaHOgbiV3VSdL7U7wz2qNRCT+kuIFNc7x9DYoAADJjknneg8jOJ+ffKNh2pjAykgAHQc0/bzYRD5he2d4ajNlddXzc1dv6jA4raqKr15tLelJ5Km3hMCXtnfZSkmQ4gzeduGM7oxph6nRI5oZ5J6yMIgcq4z1NQdNbubeuvKInYhMDKj2Du3wmsubYnDG4ctht3rmv0WA+4UNi8vsvH+cpHUeNDsW0GAajzIRWq06ZALJv+0kaDUgeJ+HfL0jpbntrVd2Ecl4+0PmbYNxHbV2l9r7d6NFRYTIhkJ++Lbj6XLAfjgmcGj54asxGRS0sLZPfNm9qTGFJMBApIuBuJ6q4VGbq/t1czWD2Hk5mUXqyS5bWWxuVsgs53H+xMpKyWe957qtxKu58KRoayeZ2fAnQvN4HHzuUlEiNSFdPUr4pJrFJoz6o+lV9qF4ZTKUdyvN6IJ7VuPnIAUnLeRYePuDwpAxaOv21+e6a4NFRm5LotWM5SkrzbT87vaIWrJT8agm25oVBVdiDH+LtFFomvISmLD20dNjm2GyMxWl866JOdk4+IikMy4x0ukpTfa2Bk0r6f84GmftZhMVZHbbksZIUCkaX7tYEVZ6nKDxyZLAQRwRh15jo0o7tcbP3/h7Injg0DZFctxWMDJXt7V3tAWtLepVcWeO1aXQNLKNHDYyqqpy/7AYlIznt/dYcVBl1NXVnTj6sYxTtsBQJJ470Dmr583/cTGEtrRRpO+CCljzYK8snzPxX+3dFZOdq7DhM8MJUZEe+uoaW/O7sFYS2/U2vGlNQtyZYldQ+9gVcbfKyzX1KEnG5Vd58FS932ZyjIU9+uK9v7I/b8/dV7Rh90fbZmdMBIdSryQhpnyJ26tAmv1bhiJDsVf3mt/Mu+xhqGz7UErSR0pacXCtrJi/9XT2y9GVbTDx2e9sJ3rhrlV/JGGweZOk0c9Db5h0XuDG8W57hvn55pLiCT0rSOmA2Jn2vwha7WiCNy8tPjaeg098pwFOd+RqEfnYTFB7pKci5TOwwRIVcb/nZX5b9erxE1fcf/vx04MdEXPpz8y+3pCACjslf32e6lvWlZUXpZhOScyHTkzT29vA2sev0Lcuv6sRMc6DSYAus72H5vd1h/tGjC3it9T129uc1IAONi6xSMYS61bWACGOR+C2iZ/OGbO/nfn8T5Ima++xIUjU7lhwbVXfSnXsXF5/sMFrjsokXUR0XlY5xGdR3Qe5iJpac5FhuX7wnXOX03nEUPEAJhbnlnu/dTKgj/MzPwnQq5bf61pahy273T/Qy81mTYIG2tkRtpxamDX8f4NS+x0nsv2Kh9eX/6Tx+pANm/OpbIdtX317aE5FbYdBTQMse1wtxU/GZ3lFfmXLxhjejsAgCzrze15x05X8kT0ZKu/0kxYeU9dPzBzeTJVZd5Vc0e4d+sXF4CDmQhBS7S9O3rwzMAtK0rH/vlHG/zmGnwhJjN3Vtas0sxr8X30yHMX5/4kprcFk0cTRrfOIzBs9Zw6408eIMScwwwXqUxlcZa6got38tYoo06ZZqmswC1VO6VKSuTrXuWmo7hzIb70iyOpgAZMOh9tt3cBQ4HHUr947qy94g4A92yq+MmzZ0AI05NlShKh5FtHe2wU972n+k80+a3kyST1u9ZVlBe6hyLRMeYnybLx1t65WtgJPLyztveDayrG+FG9Q9G9pwfNug4sn5Xtdozw8q+Yk19V6mnpjI41EkUAksauE31jF/dYUjvTHrK2m7qsJpuQa3gt7pIqXVLlxX/TFX1qMLlLArPinshx3FiV8Q8wvZmOYZnHtzVv398DzLz749hxsLePdPsCNtfK2LCkcMmsHEhZisxQ8vR2O3cCtu7rgrh5PxkBoMB9N61SSJUQYzoQQIiIx5U9R2pAMoDRo40m6mAcqfcNDSbMuQ4I2Lxi5FRxt0NeuzDPnBUEo3tPmkiNPdMWaOmJgGQl4L5pWSEg5++hwE6YduIejKb+65ETkIR3q+ilI8tLot09sdf2d9t7VUbJXevLLfrMKOzAmcHmbntyZlI637K/00qeTMpYOjvn1pUFjM/hYkxfRFX0uvrS+uZCkA2Q2bFGf59/rGH3Vw92gW4mfs2FkiHfvOyK25K3ryo13+1DHf1j7fYj9T6ImR8yuVAylDXzsWgqMo3F/YfPnD5bNwhAADhIAtKXNkbJY6+32n7V+26pcmQ5rNTeYyTmT7y4x56cmYOnB+vO+S0F3Pknb68CAlnyagLyWKy1JMZ3HpgFSRmIAEZ8Q4mG9jFmmooDpwfNnZ7V+JKZWfMqs6707yvn5Moe2UT/MxIaSuw7NdbJ+666PktFaHl1kfsdswQEmXbiXt8R/NETpwEoEAEMrGSbjR2X9Or+zqNnfPZedV5l5i3LiywmvDPyx532+FZu3d8FSd1CRoeaoXzgxjIA8CqzZZo9avYbo3zQ79l1ZCbI+nDwAVL8xNgqlDZ1hU61Bs3FrzVjw6KCq6zm5lVmLarONJezxGGM9r+6YRytH7KW4X7D/FzlmspwR1Dc7eTrD9X6h9MfGbE/SeayruXJ1Mt77DcBvntDhcWEYIXtPdVfN+7azSmNP7+73cq0PanfuqJ4XmUmADikQgerGDXsrqr6ibOlPV05IF+I4RDYdaJvTLPgE32xQMpcwF0i6xddLbhBCN2wuMCcD4FED9cPGWOo5dQ5EG3ri1lJlSFw09IiQJDpKe6vHOx64pWW8+mPMrUyPzKLKj27q13TbT6a8SfryotKvFasZijRI9obh8abg7/zeN+Z5oClWthw76bKCyoJmcpKLkZbggjYtmcecHJxmOZYoz+pjb522VNn0qFF5/mFrnWLR9mWXLewwNx7I9NTrcFTLaOPqXXN/mgoZXpByQV1Sctm5QCCTENx1wz+rYePQ0Qfnp2BRCfiqyvsRGNwx9E+e69akO24eVmh1dp7NuTMbN3faSUmo/OiEu+H1pa98xeZ6mJCrjZCyJLR2Zt94FgVKPrFk+v2/ljnaBVIkpq+u860ze/i6qyCrFFqR6xdVJCd5zRl/2tEtN1jWG3sOt4HmvnjS7qoLnHPLs8EBJmG4v7brY1793UDY8AAZAoTE5wkAEnjwafrbb/wJ++oAZlaSfdS2NFG39lxWN/Ek8bWA91W8mSSxq0ri/Oz3rVX9cizGWRcxRRQVfQ9R2pCPi8wfrFWJsKp402jbGacagnUd0TMiTvnG5eMnk1YmudZXJ1lLmeJkD11o++pHjtn7fiSsXZ+nlOVAUGmm7h3+WLffOg4GOT8tF0mE2ci7WavHe5q6bbZTn3TksLq8gzQzU/eKUmFkn8cR2HV/acG6pvN58kIAJl+6o7qS/pGLndJM68UdicEUpq08+AsoJcFoAzYeXwUrTx0dkCYskYQAE7pjlUlY/nZtYvyzdr/7qrrD0avZo8VjiXrrR1fInATZrgj01PcH3jiZFdjECgFadzlOMyHQSLRxNtHbbaacTuluzdUWI3M0K37rCdEbtnXaSV0oBkLZ+W8Z9OPEMiQF10p7C7L+rm2/NrTZZfEZC706vGmwNU/8PVD3UDMuQ6U57vmzcgey8/esaoYVDNVyyXa0Rs72Xy1wkwnW/wdfebNILmQvfKaBSjuyPQT9yMNvp8/XQ+UARHno+0TDKMPvdIkhM1H5j5+axXzKFYS3hV2sN53sjlgLSbz4p4OSzaQxr0bK5TLfjHbsYaANGK2uyIZe49U61EHUHG5VjZ0hAKRKx4ADkWTB8+YrEGqGWsW5Ga4xuTbs3RWXlmxy8TknQAkjb0nB672oNb7rFRf0sWMQnd1MWa4I9NP3L/2m9rkYOK8QdjE7KO+B1XafbhvzzGbyzOtmJO7dn6+OQfadyIzweTzliIzh84ONnaETIs7F0qmes+mysv/xavMkWjO5dnulIpw1LFtzzyQjBHHy+6+2PEr1yI/1Rro7DdZg1TA+24Y6+nTTLe6YVGhubA7HcWY81ij38ppad24YX6uqkxTB0Bk+or787vbt7zdBhIDCiBRK5Yd44cAJI0nXmux/cL3bKqwWJtJZs/tbOfmZ/3PbG+DhPnD8Ulj3fz8xTUjRDycUpGDlV0edldkvaG5sKk9/9309vd0aco40uC7ckxm2PfGxPDjzFY2LDYR3Fi7MM/csklhtecC3YORK3w+P9Lgs7asvBkz3JHpJu7xlPGfv66FuHF+2j6R+6jvwUXfPNEbs3as9MrcvbHCm+O0ou8yPdHsN3uaKRzTXt7XCaqFWtjio5tnjDzwEciQV3B4b88wxt/YMxcSMpArfDtCdx2/YnLhjto+kwdT+aKqrFllJrIJNywuZF5zPgT+wfjuupHb3NQVqu8Im14SCQFOthQz3JHpJu4/fPp07dF+YAwYAZmaO6loLwo72x54ye5aSJVFns3WrAgoMSKpZ3aYi8zsOznQ0h4Cs2fcDZ6T57pr/RVrEGc7lhHBLg26iEDIdaC26rzlwBXGp+PNwVhyhEybtt7QobMm6+rpxpr5uaaiIgurc+ZXZprLmeGw+8TIYffac0OJsPnjS7qoKfFcxQkHQXG/Dmnti3zv9yeBECBw3mxg8rQdCIAhfvHUGdsv/PFbqiz+pkxfM1mb6aW9HeYcFi/EZG5ZWVySd8WTQR55JiVeuCjsrqrakbqKzo68kWMyFybCXQOx9r7ISELpjwTNuA4IAInesdqc3SOjdMWcHHMmMxLdfbKfixF+ZfeJPrCwrksZazDDHZlu4v6th0/4OiLnbWQUCpRMssOzyvbVDTa02lw4e/OKomJrVgQyO9I4dPjs4Bh/PBLXX97babo0hwBg9NN31FzlR1xypUuaycW7teg4J6/vXHCJ5cBIi49URDs6Utj97WM95kJVBs/Nda6YbbpA3R2ri836ENQ1Bc60Bi7/l+NNAUsbQmL1vDxAkOkj7jtP9D38YiNQBhRAZpMZkLlIjBJx7fldNkdm8rMcd64rsxaZ4ZHUlr1jnbzvPzXQ2h02HZPRjHk12betLL5qQyBDXvhO2F2SjJ7+zMMnK0ZIb78synG5g5jB+VtHe80G3BfPzCzMcZntwhvnF3iyVFM+BHpUP3TZgOoPJ+o7wqZ3UwWAS1ozPx9VDJlG4v6th44bgRRQksZyHFYm7/TJna26YbOP2Ic3VIDMrKxLZLplf+cY8++f2d5mpWpzynj/mlJ1NK+CDGUBiPOXVmV939Ga8JDnEsuBK0Rm6prfuxJq6Qmf6zLtOvC+1SUWer6yyDunIsPssumNw92XTduHevvNm0HqvKbYs6g6GxBkmoj7b7Y2vrG9AyQJGIBCpsS0/UJk5mhd3yt2b6tuXlG8cFaOlYR3hR2q9x2pH91xPp403jraYyG9nXmUT95WPeoPZqhLJJIFwAkBzWBv7Z1zxSSZi5FoQ2doMHhJVabdJ3rjQTM7kwJAZesWWjvhSW5bWWTWh2DvycFoInXx3x2t90HSsHDod9W8XAy4I9NF3Iciyf9++Dho4sKpJQJkKrVPEy9ut9nhXZHox2+pshJ2JwTi2itj2FbdUdvX2Bo07SeTMlbMzl02e/REPbdc4ZQqudBkSW9qza9rKBk9JgMAjA4MxI82XHKUadvhHrM2vxXF7kU1Fue/GxcXgWRmO0cirX3R9xi3HTs3ZKX6EsAtyzHDHZk24v6dh5qaa0MgU5AAZJLeWksWcEqvHenzBZP2XvVDa8skt2zFikBiz+1sN0b7xWd2tFrJkzH4n902pmQeSkiGvIwLTVGM3YdnpkLOESwHRhicAFK8tvFdcdd042ij39wKI2WsX5Sf4VKt9fySWTnZOWaqHhICMX3bkXcjMyndONIwZD7DHcDBls7EDHdkeoh7zGiuH3ycgAQhNzAGMplyTZRpR3f4yTfb7L3q4prsm5cVWfERU2htg+/qnifBSOo1Cx6/hnBnO4cr6o2FLMdySmkkpuw4OOtqGZCXDQtHLzIhON7kO9Nu0h2BwvtMJkFeTEmu+8YFeeZiYpS+fbT3nT81dYbOdUZM76bqvKrYjRnuyHQR987oz/7l//78299/aPHqZoi7Ieo8P7+bUjDxyJYG269698YK4JYiMwl9y96Oq/zI7rr+zl7z6pPU37e6dGbpWA2tvPJsl+qoO1tSf67YhLjL7PBZX/zCUaYdtb0QNeOOYAhXprruqnX1RmXz8kJzPa+w/Wd8nQPh4T+dbgtoUc30ElMzblyQ53YqgCDXvbj7ErvaAi+mku5bbjr9ox88+vl/3FqYHwS/5/wR9qkj8So7XO87dHrQ3qveua48p8ADFlJxFLb1QNdVagE+/XabFY9fgA9vKB/7D7vkCrdc9cbuGkhJY9pNPT9SktbeaFPXeaF8/VCPuZwTnc8p91SNz1Jx8/IS4jbnQxAcSh4/d9774XD9oJV4Ghcr5+SifiHXv7hzobWEfmpA1OBsaChbYfSv7tvzi+8/9Kcf2+2WdfB7QKcmJCOtEMLj+ou7Ouy9amm+a7O12nsyqzvnP3iFwSYS03Yc7zMdk9F4eXnGxRX1xhBfoanI2n21pWPaSr3o14yYfqrVDwCDwXhtY8BcTEbnt64sJmRcb8GcisyqQrfJY1Pi7WPnLf53neg3nQQ5nOG+ADPckWkg7t3RZ32JPZTIBBijcjLp8PndpQXBL31uy4/vf3T9plM0rkDQBQKmhMSr0hNvtQciKXuv+me3V1vZQCYASePlfSPnzGw70tPaHjS93ZfSP7KpMstjLmjQ3nzbYHfuyB6/VxvYYf/pAQA40TTU74ubE0qJ3DRuS0WXKq9blGfO/lei2470AoAvFK/vMH80TOdVJe4lNThzR653cU/xodbQLwEEAKFEpkQaDrqGoo5A0LV8fscDX33qG195etacbgh4IKYCmWyJV+m5Jt8b+7rtveqtK0tmzjBZ2/P85J2+sLstNZJNyqsHuky7TgoARRrRvf3qzC6rkC2UH2F036lBGC69ZCpb3BDZOaotCSe3rCg29wsSbeyM9PtjJ5v9g4Nx00cxNL5oRpbLgRnuyPUu7m2hX0X0s5RIFGRKlHe+FCVCCDIUdOsau/PW47944OG/+/tX8nIiMOSGlDzJ+k7hD28223tJj1P68IZyS5EZeqY9dKzxvaeZApGUlVrYKX3l/LwbF5oOGsytzJxTbvrAJ8i0sTPc748eOmvSDz1l3LggvyTPM/6eXzU3X/aYC7vHgsn9p/tPtQStnPsVfO0ijMkg17u4h1InOyKPUiIBMEJkgPcqESUipUmDQ16Xqv/dJ3f86gcP3Xn3AUUQCLiBk0mTeAd7u7bX9sLZ92yqBJcMZkv6EQIx7cm3Wt/z13vr+js6zddd0vg9GyqY+QARo3TZzGxzPosAwMhQMPXcztaGzrC5Y1YGX2+TRM6rzFxUnWmu5QJe2d91pHHQdCRNADilm5cVA4Jc3+LeEvppivsJUEIkSkaux0GIIETE4srAkKe80P+f//zSD/77d6vXNJCoA8JOgMk4xcpowJd46s1We69644L81XNyLSW8sy37ut5TTuT53R2mYzJcOLMdd2+ssNb+DUsKrNwLQn72x7O9Q0lzNr8Otn6xPXWlCaEbFhWYW3Mo0muHu9880gOqyQp5Bi/Odc4uz0DxQq5ncR+IvdEXe5USGUCiRCFX/TqECAAIRZzhiLpuRfOPvvn4l//tuarKAfC7IK5MQrqkzH738rmEBSEedfJupTYTa2gN7LvoNJM/nHplf5fpmExS37ioYG5lprXGr1tYYC6+cWF1Vtcc1U39ls5ryr0r59jml7tucYG5l0kiLd3xtt6EhYD7kprMLI8DxQu5bsXdEPGm0E84JAEoBXmM9o+ECIPToYDbMOiffuDog997+DOf2ZbjScCQF1JsQqM0Kj3V4t97wubC2R+5qdKV7TCt7wRA4y/ueTdB8/VD3Z1dIQvH4j9+a7Xlxs8pz5xb6TUdmQEwL5HGytk5NrpurV9UmJNvsuQhtWSPwbnp/VsExf3aoivyRCB1mBKFwHBAxsR3IURomjTod2d5E5/7y7d+8t2Hb7vjmKwzCE5wIJ6/tM/mhPeaUu/6hfmWTCLpln1dscT5yMyrB7rArMbqvLjU+0Ez6e3vlWjG1szLs2KCZp7NK+x03SrOdS+ZmWWl202OneBgN2KGO3Idi3tC72oN/ZIAJcAYKAQkCxchBKIx2ed3z53R/+0vPXv/tx5fvKQVwk6IOgDEREi8Ij29o30gkLD3qp+6o8b0nioASKypM3TwzCAADAYSrx3sNl0LO6nfta48L1MdT+NXz8uDdBfN4kL1ypuW2jz/ff8NpVYcIEwOn5XFnqUzMcN9LEtRFPdrk+bQT6J6MyEyBYkQ2Zpj6rC+A0Aw4ozGlc1rGn727d998V9eLCnyw5AXkkra9V2mXS3BZ99otfeqH7yxrLw8w3RwgwAk9afebgWAXSf6e3pM+skIAIXdaz69/T3cuCBfzlCsnMgfOym+bHb2nPIse6+6dkEBqCy9A5POF1dnoaXM6A+jMLATrklxDyQP90Sfp1QlwAiRCWHjvCAhwjCoL+CmRHz6nv0PPvDIR+/b7Rr2LTBoeicBEv2j3Q7v2V7l1hXFVkIEMnv7WC8AvLy307S8poxFM3NuWjbe/JM5FVmzSr3pjczoxvrxmYWNyMLq7PIiV3pbzvma+dNo2m7Vs5sI0FDcr0VxF02hH2siSIBRIlMi27UEI0QkkvKgz1uUF/qPz2356f2PbLq5jiVlCDlBpC0Q72B7z/nq20P2XvWTd9SAYn4WKbOz7aEn3mzZebzPdExGN+5cXy6x8T5REmNLZ2alVyIlsmGx/eKe6VbXLyqwckJ47Gsjh3Tz8hKYNhCrAsVFEqY9156498ZeGoi/RYlKgFGQAaiNIVpCBBARiTr8QdeSuV3f+9pT3/rKU7Nnd0PAfcG3wO7vw0gkEH/0tSabQwQL8+fMyDQtNAQA4Gu/qW0fiJmLyXAhe9WP31JlS+M3LilMY9hd54VFbqt19UZh3cICK7sdY8TgRXnOuRWZMH0gViogE0J0HkVxv8bEXeOBpuBPAAQAo0QhRErLE0WEECQQcsUT8gduPvnz7z76D/+wtSA7AkMeSEr2T+Fl9tiWcyFbfcQcCvvoTTOs5BRS0tARSpmdOKeMG+blLajKskciFxUyd9rC7hpfXJ2Vm+lMx7XXLy5gHjl9LV9YlZHtnUYZ7hSs5aoSnQdR3K8xcW8L/yacOsHO76Mqad0TJ0RoGvP53S419Tef2Png9x+69yN73VRcCMTb9wLLtK0rsuNon73tv3tDOXFJltJmzG8zGOL/3FFjV8trSryVhS4rR7HGtMjgm5YWpOmZWVidM78yI10xJc5vm2YZ7ow4LL3jVBNhFPdrSdwjWkN75GFKFQISMZnYblXfgRCIJ2Sf31NWFPjKP778o/sfWbf+DI2pEHIB2OguKZ58y+bae8vn5G5cbMnh3Xygo6jEe6eZ0hxXx6nKq+bmpCV4LQS4pDtWl6WpJxilq+bkWlkwjd5yAJmtmju9MtwZdRHzcVcCTDOGDJGA6c21JO4toZ+ljAECElzZRiZNEg8AkagjEHKuWNj+/W888bUvPVtd0wd+D8RVe/TdwV490tXWa7OP2IfXl0/EgaCksXl5UUGWneGCzcuK0hK81kVlgSutYevbVhWn5a0yRG6uOn/GdAq4A0jUS4li/oVlCaM3preiuF8b+BK7eqIvUKJeOI860YcUhgPx/qBbS7G733fsl9976DN//XqWNw5DXtDG7VsgUV93+MnXbTYBvnN9uTc3bfGNd2aUEv3EbVX2XnXNgvy0BK81Y+3CfE8688RvXFjozVbt73PNWDUntzDHPa3kSaE5jLiEyaPSBKjOw4Pxt1DcrwG4SJwLfo9DAoARkCnIk3UCjRCR0tjgkMfjTH3+L9/65fd/+8E7D8oGvWAgPJ4Hmb1gd+296hLv5uVFkNTT2CM6ryj1blhsc/LJvMpsK97uYxiL7lid3lTCykLvqrk59vsQGHztojyYZkjUK1GPEKYfA0Yc3dGn43onivtUpyvy1FBiPwWVgp2J7ZajNMMGwoNDnpkVvm9+8fn7v/X4kmUtEHFAVB2eOVgT9wNnhw7aXTj7vs0z0tsdKeNDa8sy3DZXBZIltrja7mx3Q7izHbaPQ5ezYVGhlUrlV18eOdnmZdMow/2CRrtkmgWmTY6AEClh9J3xfylp9F2hR/W43ulL7OyKPt0W/m17+BF/8qCA6+pcqzT1m5g0BlpCPyUgCBBqx3lUu6bwABAIOSVm3HxD46pFbS9tW/LYU+s6W/PBlQJHCoRJjafEiGu/3dq0er6dE7TNK4oLij39vri5vPWxi45MPzJuy4ERuXl54RNbbY1TacbSmrzqkrSHrTctKwQHA2HfJMTgxfmu+TOypt/skygs35rmMuLyJw8f6v9YnmNTprJEZYUCRJL3x7W2uN4e09sTRrfGgwI4CA4AlChZ6qrZWf/PI89BcZ8gWkI/i2iNjLoIkYilQw1plfhhA2FV0f/swwc2rml4/Lk1L7y0MjLkhYwYSIY5iVfZ87vavvFXSwqybducLMh23Lm27NfPnAEpDYHmlLFods66xWnJLNywuEj2ypomLB5Bvxydb1gyEdkmN8wvqCn3NrVHTBsmX3FY4ktnZU2rDPd3cEqlQnBLL71gxKlxf2fkiS7y5HCNNgEGCE4IG3YuYcR50VJbDCX2Hhv4yyV5v8hQFmJYJu2EU6c6I49TqhAiD59KnYqzCyKSGvP5PflZ0S9+9tUf3//oxptOsrgCIRcIM+mSEu3rCm/da3Pk/Z6NlSCnx9BK53+6eYYipeUpqinxVhe5bYvMCAAn+9CNZRPwPLhUefUcW8PunN+xatrFZIbxygsIsT66E5Ak6mHEzYiDEYdE3BL1MuKiRL0sl5pI1JPiQ2f8XzFEDMU97TQEvpPiA4TIlCiTHm2/6jMEABCJqYGQa+nczge+9uQ3vvrU7FndEHSbS5ek5MXdNu8C3bqyeNGsdGzxCVeWet/mqjR1qSJLG5cW2JYzrvPKIveSiTLL/dC6ctveLQHgkNbMn6Ye7l55vkS8AvjEfBwjrnDqVG/0JRT39NIX3zKQeIMSJwWFEsVeG5k0TeGFIIGwM5mUP7S57sEHHvmrv9rmdSZhyAv62NIlHdKrh3tOtdh5eFqW6N0bK+w/E5TSb1paNLPMm77+vGWZfTnjKeOmpQWeiTLL3bi4KCPHYU9CpMZnlnsW1+RMT3F3SuUqKxZCn8C3mPqSu1Hc04ghYk2BH3GhU5AoUa26TEyKxIOmU5/f41S1z/3FW7/8wW9vf/8RSWMQco3uLslIPBB/cpvNCe9//v6ZWfkum1M4gHzmQ7PS2pMr5uY57fJ2p/An6yom7BkoK/CsnJNtz2pJMzYvK7KxIqDJh1liRBGmJ1Vi+EiKDfeNKHnODRPp8khASug910HmzNQV9/bww4HkYUYclChkCgdkrqTvhIh4Qh4KuGfPGPjOl5777jcfX7T4nTJPV/02CnvqzZaErVGUqmLPn91aDXH7pj9Jfen8vA+kOYQ9szRz7YI8GxwUdJFX4Fy7qGAin4F7N1baU5iJwublRZP1JDPikKjHbDKiAC5RLyX2DEhFrrtkmjnhaivgGmeKintcb28N/YIQmZyftkvXYueeL/MUdoYj6uYb6x/8zu+++M8vFhUER3GXlFl9e+itIz32Nubv753rzXbadnJSF/9w7zxVTvvz86G1ZTYsOJL67SuLiyf2eOcHbyzLyBt3h+u8qMi9ecUk7qYSp1QhwOy0wFCZbQOSR55d4Lrd4BO0ySnAcEiF5NrUnGtA3FtCD8aMdkrU9Pn6TqDEC4NTX8BDAD519/4HH3j4ox/b7aL8iu6SBEDnf9jWam8z5lVm/sUHZ0Lcjgo1cX3l4oJP31E9Ab1398bK7AIn6GI8LytI5L5bZ0zwfa8sylg3f9zLjqRx26qi/PQYFI+RTGW5MOnzI4TIVBbb2IYZ3r9VWSEXqQn4vkIYWcoquPaZiuI+lNjbGXmMEQcFmU3V9EcLEp9ISoNDntKC4Jc/v+VH9/9u7fozLK5A2DlClMYp/XF7e6Pd5Zm+9OnF5RWZ4w0EcwEy/ebfLJeliXh4Kgu9n7q9ChLjGJNSxtyazFtXlE78Tf/0+2vGZTokAGTy6ffNnNxHN0tdIVHP2PNVBHCZZeY41tvYBqdUNjvr3wUY6Q7OcJFySuVF7jtR3NOxJuJNwR/qIkJBYdf+tP09URpCIBx1BIKuFQvaf/CNP3zty89UVQ7AkAcS8iUqQEk0EH/jYLe9DSjMdnz/c6tAwLi2KCOpf/zYgvetnrhAwd/eM9czHisuzfjCfQsmZUPynk0zli/Itb7VEdPet6ZkUoal90RFch3rxx4VMXgsV13nkmw+t1zo+sDsrH/nImE+RjT2ObsuwJiV9UWFXg+5SezrX//6lGpQT/SPzaEfM+KkxMGIkxAK1xeEAABJJBUCsHhe100bTjNVa24qTvk9oHBg/Pw0Xoiozv/8/TbP2ubPyNKE2LWvC2QKFs6GhFO3bKr8zb+tldnE3Ze8TGc0mdq1twtUyUKD77ip7IG/XzWegzDW3y5Ka8q9j73eLIT5Us86d7mkh7+yvjRv8p0gnVJJX3yrAGPUIgpCaIy65uX8l8rsT8zPVJY4WOFQYo8h4nbt1r47JokYIcqc7K8Uu+66PqRmaom7xoN1vv+rcR8lDkad9FpLkjEl8Qan8bjqdac2rmlYvaopGFPam4pEQgHFAApASedg/H2rS0rzXfZ+9C3Li8OG2HeoB6gZxeECItotmyqe/sYmr3OiZ8FrFxZsP9XX0RIExUyMLqbPnZ359LduynJP2sH96uIMt5u+sasDJDOjKReQ1L//z6vv3jBjKjyuKiuSWeZAbBsh7Cr6LoTGwZiX85+5jg1paolXWZDtuCGqNcX0NgBBCBunRAjgXMQFaNnqDQtzvpvvvPm6EZmpJe4toZ92R59h1M2Ik1EHgett2n6Jvg9nxGtSMqGUFgZvu+n0jKq+9u4cX2ceAAGFi0iKUvYn68tt/+g7VpUUFXm2H+3RQsnRRUcAJHQA+IdPLnr039d5HJMQKJMZvf2Gkjdqe/s7I6CwMb3OEa2q2rvlu7dWF09ygYu1Cwupg2zf2w1AgI2h6bqAhPblzy79f59YMnUe1wxloUQ9Q4k9HFL0MosnAdzgUUYdc7O/XuK+O60tcbCiItddTqk0YXQnjQFDxAQIQoCMtYC9EGAIoXFIcpGSqDtbvWFW1r/WZH7ewQqvK4URYqqkc0a0+v29f6LzEKMuRtwWKrBcuwhBJMnI8saHQs4nX1r15HNrhvqyQIlVlTuOP3qX15WWmXJ9R+g/flP74q72VDAJjIBEgV30enABOgeDE4e0ZmnhN/58ya2TXcCzzx/7y/v3bH2zHSgFBxt5TBIASR10vmldySNfWl9Z4J0it/jXW+r/6SeHI4MJcEpXdOg0BMQ0NVP+zt+t+Py9C6bgg+pPHmgJPRhMHTN47MKjIgBAppnZjjVV3r/1KvMn8K1JBbWTg/G3Qsm6uNGZMgY5pITgMIIhpwAAIJQAoaDILFtlRW5pRpa6Mltd45TKrktVmULiXjvw2e7oE4xmStTFiPN6DchcZYIsgDhVzeNO1rcWPPrU2ldeX6IF6TM/XXvvTeXp+9gzbcEX93buOzVwujXQ60/oBgcASkm2R5ldnrFqTu6Hbixbt7CATI27IYT4w1vNP3nu7IFTgyJuABfvSrwAoAAOtmJuzt99ePanbp81kRsDY6GxK/CDp08/+Xb7UH8cDH7JEy4ESDQjx3HX+tIvfGzB4urcqfykhlKngqnapNEnhEaJwyGVZKsrXVL1JLZJ48G43hY3ulKGX+dBQ8TecSwghFGiMuKSqFem2SordEplCs2C6zowMIXEfSD+5uH+TxJCGHFJ1H0dnCCwPIUnRHjcCcaMvUerf/yrtaXedW/9fCLigEnNGAql4klDACgSzfYqHucUvQsG56da/cfP+Ru7Qj2+uG4QSnlRjmNmacaSmpxF1dkym7rps/2B2NGGodNtgdbecDjGAcDjpDOKvHPKM1bMzi3OdQOCXDfiLoR+oO/uoeReiXgl6qLEOc3uwvAtEFwYAnQBHASTGcvK0IKRzNfe3PS3t325rCADH1YEQa4xcW8L/+bk0Bcl4mbExaiLXO/LJQAhhM7BABjeBQIBgoKssDynVO6UylSaJ7M8heZnOUoVUsQhixGGDyuCIGNn8tfdSaO3OfhjChIBmRLlulN2IYBf2OSB4W16SlSZ5jikYpUVKjRXptkeeZZbrlJZoUQ85LIEXtR1BEGuPXFvDv1vVG+TaSajqu0HEyYcLkCcn5gLDQAoUSTqlVimTDJllumWatxSjVMuV1mRTDMZcVCiTrutYwRBrntxD6aOdYR/L1EXJTIlyrUmc2JYyrlICaETIjHilIiTUadMs1VWnCHPd8s1CsuXSYZEMyTqnVb5nQiCTF9xbwr8UBMBmWRNeYOw4VGHc6G/Y11EgFIiU+L0yHPccrVLqnKwEpXmqazAIZWhjiMIMk3FvS+2tS++VSKeqVqOQwjgIIQAzkVCAJeIR2X5DlasskKZ5TpZcYay0ClVyCznGioUhSAIinsa0Xm4MXg/gKBEmTLTdgEAXOgCNAEGBWU4lqKwHJc0wy3PylAWOFmpRDMpdaCaIwiC4j4CbeFfBZNHZZpNQYXJ8/UVYAhhDG+EEqCEMJXlueVZHnmWU6p0slKVFaqsQKJefFYQBEFxH4WY3toa/hUjTkJkMtHpj8O5iToXSSBUplmqVOigBSorciszM5UlTqlCodlw/efaIwiC4m43TcEfJPQumeZQotL0T9sFcCE0AZwAJUSiRFVYSaayOFNZ5lZmqbRAppkS9WBKIoIgKO7W8Sf2d0WflqiXUYWdT3+0/ZSsuHCaPyUEZ9TlkMrc0gynVOWRqzKUZS6pghIVbz+CICjuNomu0BuC/8NFQqJZhKgA1CZlJxdsmg0ADkAIkRSW45FnZ6s3ZMjzHVKpzHIlgq5MCIKguKeBzsgTg/HtEs1gRLUr4USAYfA4AJdYlkuqckmVTqkiQ5mfpa6+PmohIgiCTGlx17i/KfRDShRK1PGdR31nkg4CDIl6sxzLchxrvMoSl1Sh0FwMuSAIguI+cTSHfhrVGmWWzYhq3rGdnD/oDykCskQzHFKRR5mdq27MUBY5pTLcDkUQBJkEcQ+nzrSHf8uI68J5VBPzdC5ShogTIrmlqkxlqVdZ5JFnuqQZCssnmLOIIAgyieLeEPjvpOFTaS6FUc+jDu+O6lwYBAglzCmVZSrL85w3Z6nLFJqLk3QEQZApIe79sdf64q/KNIOMltj+zu6oygrcymyvMs8rz81W16isAO8WgiDIFBJ3LpLngt8VwClRGVVGOvwpBAguklykZOrNUddkO9dlKyvccrVMs3CejiAIMhXFvT38iD95UKY5FwzC3j21JMAQIgUgKHFlKktyHRvynZtdUs21X7UDQRDkuhb3hNHdEvopJS5KFArnfX0FCC4SAEKiGS55doa8IMexLtexAVMYEQRBrg1xPxd8IKa3yDSfEhUIFULTRYQSNUNZmKtuyFSXeeRZKivE2AuCIIiNECFE+q7uTx062PthAEKJkxKVAFDiyFJXlHnuy1ZXMzQDQBAEuRZn7o2B+5NGv8ryFZbrlmZmqctL3Pe4pBnY7wiCINequHdGft8bfT7bsabAeVuuY2OGskihudjjCIIgE0C6wjKGiJ0e+pJXnlfgusMplZHJrsSNIAiC4m4DuogIwWWagV2MIAhy/Yg7giAIMomg6xaCIAiKO4IgCILijiAIgqC4IwiCICjuCIIgCIo7giAIijuCIAiC4o4gCIKguCMIgiAo7giCIAiKO4IgCIo7dgGCIAiKO4IgCILijiAIgqC4IwiCICjuCIIgCIo7giAIijuCIAiC4o4gCIKguCMIgiAo7giCIAiKO4IgCIo7giAIguKOIAiCoLgjCIIgKO4IgiAIijuCIAiC4o4gCILijiAIgqC4IwiCICjuCIIgCIo7giAIguKOIAiC4o4gCIKguCMIgiAo7giCIAiKO4IgCILijiAIgqC4IwiCoLgjCIIgKO4IgiAIijuCIAiC4o4gCIKguCMIgqC4IwiCICjuCIIgCIo7giAIguKOIAiCoLgjCIIgKO4IgiAo7giCIAiKO4IgCILijiAIgqC4IwiCICjuCIIgKO4IgiAIijuCIAgyxfn/BwBcPl/tlLTHbwAAAABJRU5ErkJggg=="/>
		</div>
		</html>
		';

			print $w;
		    exit;
	}


	public function callback(){
		// call to notify

		$trxid = Application::getInstance ()->store->read ( 'trxid' );
		$ec = Application::getInstance ()->store->read ( 'transaction_params' )->entrancecode;

		$status = 'Open';
		Logger::log ()->debug ( 'transactionstatus is '.$status );

		$merchantKey = Application::getInstance ()->activeMerchant;
		$merchantId = Application::getInstance ()->store->read ( 'transaction_params' )->merchantid;

		// recepy; trxid/ec/status/merchantid/merchantkey
		$sha1 = sha1 ( $trxid . $ec . $status . $merchantId . $merchantKey );

		$params = [
				'trxid' => $trxid,
				'ec' => $ec,
				'status' => $status,
				'sha1' => $sha1,
				'notify' => true
		];

		$response = RestRequest::doGET (Application::getInstance()->config->read('callback_notification'), $params );

		Logger::log()->dump($response, 'notify_callback to '.Application::getInstance()->config->read('callback_notification'));

		$status = Application::getInstance()->config->read('transaction_status');// Success | Failure

		$params = [
				'trxid' => $trxid,
				'ec' => $ec,
				'status' => $status,
				'sha1' => $sha1,
				'callback' => true
		];

		$paramProceed = '?'.http_build_query($params);

		switch ($status){
		    case 'Success':
		        $href = Application::getInstance ()->store->read ( 'transaction_params' )->returnurl.$paramProceed;
		        break;

		    case 'Failure':
		        $href = Application::getInstance ()->store->read ( 'transaction_params' )->cancelurl.$paramProceed;
		        break;
		}

		print '
		<html>
			<div style="width:600px;margin:20px auto 0px auto;">
			<br>
			<h2 style="margin-left:77px;">SISOW DUMMY Environment</h2>
			<br><br>

			<div>

			<a style="display:inline-block;padding:15px;background-color:#e0e0e0;border-radius:5px"
				href = "' . $href . '">
				Click to proceed as ['.$status.'] status payment return...&nbsp; &nbsp;
			</a>
						</div>


	<img style="display:inline-block;float:left;margin-left:10px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAfQAAAEJCAIAAADtuEIaAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAR5pJREFUeNrsvWd8Hcd1939mZsut6L0SAHvvolglqrlFsiQ7lh3bT4qTv1MeO8VOnthxie0ktiz32Jarii1Z3WqkGiWx9wISbACI3svF7XV3Z/4vQEqkCBLYxV4AJM73oxciCeydO7v7m5kzZ36HCCEAQRAEub6g2AUIgiAo7giCIAiKO4IgCILijiAIgqC4IwiCICjuCIIgKO4IgiAIijuCIAiC4o4gCIKguCMIgiAo7giCICjuCIIgCIo7giAIguKOIAiCoLgjCIIgKO4IgiAIijuCIAiKO4IgCILijiAIgqC4IwiCICjuCIIgCIo7giAIijuCIAiC4o4gCIKguCMIgiAo7giCIAiKO4IgCILijiAIguKOIAiCoLgjCIIgKO4IgiAIijuCIAiC4o4gCILijiAIgqC4IwiCICjuCIIgCIo7giAIguKOIAiCoLgjCIKguCMIgiAo7giCIAiKO4IgCILijiAIgqC4IwiCoLgjCIIgKO4IgiAIijuCIAiC4o4gCIKguCMIgiAo7giCICjuCIIgCIo7giAIguKOIAiCoLgjCIIgKO4IgiDTGQm7AEEQUxg8mjD6NB7gECegyDRDZQUyzcaeQXFHEOQa1HQRH0rs7Y+/Gkye0LjfEHEADkAoUSWa4ZaqC5y35btuUWg+9tVUgAghsBcQBLk6/fHXWkO/DGunAICCAoQRIAAEQACAAIMLTQjdIZWUeT5e7vkkI27sNBR3BEGmLjoP1Qe+2RN7iYJEiXr1Hxag6zyaqSydn/NfHnkO9h6KO4IgU5EU99UNft6fPCBRLwAZ428ZIqrQ/EW5P8xSV2AforgjCDLl5uy1g58NJI9I1GP2d7lISDRrad6DGcoi7MlJAVMhEQQZmYbA//iTBy0oOwBQ4tT40OmhL2k8iD2J4o4gyFRhIP5mT+wFmWZYvYBgxBXRzraGfoGdieKOIMiUQABvC/8aAMYeZx8RRt3d0Wei2jnsUhR3BEEmH19iVzB1ghHHOK9DgGk80Bt7GbsUxR1BkCkg7vEdQujjnLaflxiiDiTe4iKJvYrijiDIZCKEHkzVUiLbIzFEjuvtYa0eOxbFHUGQySRh9CaMbkLs8iYhhkjEtCbsWBR3BEEmE40HDJEkNoqD4Ck+hB2L4o4gyGQiQBPCsPeaXCSwY1HcEQSZTAhIhNisDIQo2LETDFr+XgMkUoYvlEwkOWPE65KyPAqj5Nr9OrohAuFUOKYJAK9LyslQr+mvcx2KAs2gROUiRcCm+0KoYp/buy6iCb1r+OCrTDOcUikjHrxrKO7Xlgjy1w71PPp604HTPl8ooXNBAZyqVJLnvH1lyadur15Sc42VRzjTFvzN1nOvHuzu9sUSKQMAVJlVFro/vL7iTzfnzK/IBpgSPrG9Q7FTrf6m7vBAIKkZhIBQFVKS65xVlrFgRnaGa0pMQgORxKF6X3170BfShCAOBapLPCtm59aUZI7zyg5W5KCFUb3Fpum2YKC45KrxXyiqNXdGH/PFd6W4bzi3khJVobl5zpvKPJ9wSTNQNC4ZUtE4bGrS2BX+6wf27djbCQJAoUAvLJOFAIODxolL/sInFv7PXy+7Vqa9Dzx1+ssPHk2FkqAwkMiFHGoBOocUyJnGX3/U/9VPLSx03w7AJquRe072/vCZM68f7g35E6AL4Be9HZSAQkuL3ffdXPn5e+eXF0zabFHTjR89d+p7T53t7Y6CLuCdV5gRxSt/aG3pf/750oVVOeMahv3/0RV9WrJjRsyF5pRKVxc+y4hrPNfpiPy+KfhDnYcocZDzbvLD19e5SMg0sybzn8o8n0DpQHGf0rx1rPdjX90x2BcBt3KVlwYiqb/62IJf/+uNU/8bffHBow/8phacDNgVgrkGgThfta7uF/+Wtaz4XwHkCW6hL5T4x58e/P2WZkhxcEjARhoyBYDOIaF78x3f/uyyv7tr/mRM2JMf+8b219/sAIcMymWdaQiIa2qG/N9/u+yfP2LdjrEv9urJoX8apxxfiKJEyj2fnpP15fFcpCn4g5bQzxl1kisEGwToBo9Xej8zK+tfUUDOj/Vf//rXsRemFIcbfB/6wptD/ji4ripwhIDCjh3vryj1LpuVM5W/0Y+ePfPVnxwCl3xFZQcACqCQ7nMVxzoab1nbmq2umsgWtvSG3v/FN97Y3gmqDCqDKy2GCAAjoLJU3Ni6vT3K9dtXlk5kOw3OP/a17a9s64AMdeThhxJQmKHB6zvaU4zfsrzE2gc5aflgcnvK6BtntrsAzog8K+PfHVKB5Yt0R59tDHyHUTe58pKOAKVEGUrupYRlq6tRRlDcpxyxpPGRL7/d3BYE5ximroQAwMlm/yfvqHGpU3T75FRr4ONf3akRuJqyv4NqdDWVE/eh9y1bADBBOwq9Q7EPfOGNutN+8Chj3UFkBCS690APVWHT0uIJ68xvP1b74ONnIWO0UDglILHdB7pnV2csqrYy8FMiUVD6E2/Q8YXdDREpcn243P1xcf5pNR+D4v5TQ1/gIklHH2YIJXIwVZvrWK+yAhQTTIWcWvz6pYbDtX2jzNkvRmHNLYEfPX1myn6j/33mbDSYAGnMT5oqHn2+5nDn1olpnmYYn/zmjpOn/eA2GQiiBFzy135x/Im3J+jsZWNX4Lu/Pw1OaazNk9iXHzzmj5g2dRFCcC6KnHfnO27VRdhyg7lIOFl5lffvBYAQ3OLQG3sprneOcYwhwHQe6Yo+g0qC4j610HT+qxcbQDG5nahKv93SOBicisZM/f7EczvbwGFmVSEb/r7iZ7Y3APROQAvv/0Pdm7u6wWMpxE8JEPJPPzzUNRidkGHyTGDQzDCpsNa20DNvN1sLpxBC5mR+xSPNMUTMgoMYFxoljjlZ/+lkJVwYhFjc9h9M7DQVGqLE4U/uM0QU9QTF3ewjK5KakaaLHz/nP9kWHGGXbBQ1pF0docdeb56C3fXy3s7+vqgJPTo/AZP21MpD2oF0N69zIPKjJ86YG3suE9Deruh3/1CX7qaG46kXdneabiohW/d1WX3UDQcrWpTzvy6pShchs3N2QqR5Wd/Oc6w3xnHYVefhuN5OwZS4s6QxENc7UaxQ3M1xrNH3wBMn03Txc10hSOpWYpMSfeyNZoNPucSnp99utXIOhvGegcyOwI50N++RVxoHemMgj+8tcEjPbm/3h9N7vP54o6+t1/wwKbFT7cGkppkcEc7fM0MYbqlyae6vc9QNughzMfp1BBg6DzlY6eKcnxU6bzP4uGZCSaNfM/xATK1lqSESKWMQxQrF3RzffbLu58/Wh6JaOi6e1DhY02dVOnR6YNfxvinVV+e6wrvq+kGxMi82DKc/UatxX/qal9KNp95uMx0EG2nl1NkZ2bq/I62duWV/JyQM0yMlBV9Ii8a55c81hOFgJUtyfj4746sqK9BF2BAJAcblYRwuUroIU6KWeT69PO+xXHWNMW6DmhQfMkTc/EFZnuJ+FCsUdxNsP979zI72rrPBZ7emJQaS5VEsphQQgBR/ZnvblOquLXs7okPxkTP2Rpn+kUyvRuWeQOJs+pq3/1T/icaADeIOAAJeOdCd1s7cf9pneto+/IZTK8/UxSFyLgwCcoXnkyvzn5qd+R+ZylJGXFykDBHVRcQQUUMkKEguqarC/dcrcv8wN/MrKst/j7Jbi7kbIiqAm434C8F1Hka9QvuBsaLp/D9+c9QIceD098+d+4uPzrH9I6qLPcwlGYYAC4dOFfbC7o5vfmZZtneqODS9vK9rTOmPI0y8SHFeWJISvsTefNe6dI09+zogZYBsh7irbNuRnoFAPD/LmY6m+kLxM21BK+LOIS9DcTtNf0dCLjneKIAbAmSSXeH+dKnrE0mjN250JYxOnUcoUR2syMHKHaxUpg4BcPmE3fJuKhdJsLKYFYaIo2ShuI+Vh16t33OkH0AGpzjQGDjXEZ5Z7rX3I2pKvRUF7pbOsBVxl2lnV+j1Q90f2zxjKnTX2bbgrhN9oFqUzrkzexllYe1U2oZq46W9XfYoOwBItK83vvtE790bq9LR2qMNvr7+GMjm31aDVxS6Fcn0L44ox8MSD8CcUplLKgO44aJ/AiHEleIw4xD3lNWlVAqmPRiWGRODwcT9fzgFGgUhQIWonnpmh/0xEJdDWlKTDZazcTi8drB7ivTYC7s7kqGklVFKEOpMLZnbaehqXG9P6GkJnh46O3CmNWSbuAOAIV7am64MjdrGIUhxK1vTXFQXWZyCUHolceBcGMal/3FhCOBXUnbL4i6EbmnmDmPZ+0VxRwAAvv90XVNj8LyhlQTgJI9saUgkdds/6P03lILlnBeVvXqwa4okvL+8t8NiTEZn5cX+8hK/biga7w+n0hJ2f/toD8R1sNFyTab7Tw+m9LSkyR5v8oNFezixeGaWtQ8djyiPbZAYywQcna9Q3NNJ7bnBnzxXD0QCLoARkAg42dn6oW377J8mb1hSqGYoYC2pUaI9PZHdUyBn5mRz4GC9z+JepcY2rDqX4YlzTgVoES0t4r7jeL/FseeK4s7OtIWONtifgZfU9EP1PpDMd6YAcEorZudZ/ujxi/t4lB0ABOioPyjuaeRrDx+LDKSAEyAAEgAFEAAG2bLb/mX4vMrMm5cVQdLqBNAQz+5sn/Qe27KvMxW0GJMBVduw8pxh0GFxCafqbW9ee194/+lBe/Jk3hVCgJi+45j9p2qbukLN3VGQzHemwfOyHNUl1neGCCHjUWdK6fiHB5y6o7ini+d3t764sxOGD0AzAuxCXpZLfulAT9+Q/UdX7tlQAZaPI6nszSM9Q6FJjsy8cqDLWt4eaGzezJ45Nb2plAQAlMhh7bRuJO19/2vPDYUDKSs5mleH0d0n+23vzCMNPj2qWRkpdT671JOXOa4EHkIIY8yCRtui7AiKe7qIJfVv/u44xAH4sBPTRR3moF3dwWffbLH9Q+9cX55f7AHd0sETifb0RnbUTmZkpq7Jv+dk/3hiMm5XigsCAASkFO+K6aOtRUyq+/baHjDSMCNU2J6Tgz0+m11Ndp3oB2uHkAy+cZk95oimlNryeICguE8cv9pSf7R2EAgDAsDekzgqQCaPvWa/I2BhjnPTkgJIWY/MPP32ZJ5m2nakR49YmmlyInsTa1c0p7R3BgZqiGgwebWESLPFZrjgbx3ts7iwGOVlIn5f4vi5IXuverI5YHGRweiN821zvqWUjirZw7I+zjg7guKedroGo995/CRwBhyAwbsBmYtiIIdO+46dtf+I/MdvqQbLxfNU9vrh7h7fpB3ieHp7m0UxSknLFnTMmtGnadJFeiFCWu3VZ+2m5ogtPeGGzsh4/WRG1jYAXew4bmfYvd8fa+gKW9lNNYQrQ1lUbbMn/rDE0wsMZ9QM/z/KOor7NcO3flfb0xYBoEAviPtlMzUtajz5hv3T5NtXl8ysyrI4eZeorz+6o7Z3UjrtaIPv4Bmre5WC3LHhtCwbF0/GCUhR7dxVpufEZFBmT11f3Npm79g6/43DvTbuAh5pGPQNWLJw0PiSmVlVxRnp+JbkAhdLPCoGivu1wd5Tfb9++RwQGWBY2a9gXuSWfr/lnM9v87aqxyndubbcemQG4LlJypnZuq/TiKSsSKdBs/JDyxe2J1PypToiJYzOhD545ZiMuc9683AP8LR9f5meagnWdwTsul5tox80S8eXDL5mXh6+yCjuyHtF4+sPH9ODHAQ5XzbzSv2kkq7+0PYjPba34N6bKsAlg7Xy5Qp7+2hv39AkRGa27LeaJ5OU161oLi0K6Dq9dGIuaXwglDp95VmkiQ+JxFO76gZsToK8dDGXCCZttOc8dm7I4iKDwqp5ufgmo7gjl/D4tqY39nQDZQDikvTHK6zE02FFsHZhweq5uRYT3iU6OBDbloYhZ5QYQr3vwJlBK34yAkAyNt3QwPmI/2jbUaa6Zn9LdyQtu6kXDTd76gZsuVA0kTp81mfFI8EQnmz1xgVYRxTFHbmIUCz1X787ASkCYrgO8midpLJX9/e0dUdsb8ndGyqsZ+wJ8dyOiY7MvH6oW8Q0Kw6zujSjcmDFwrb3xGQuzM1ZRKsf6Suarv524PSAFVd0k8umnSf6ogkbjKvOdYba+mJWAu46n1uRMaPIm75vKcDQeVjjAYNHIY1xLgTF3T6+/9TJ06cvFH+hI+2jXjZNDvTFHvljo+0tufemSmemavFAk8J2n+zv9ycmrN+EgOd2tls+u3Tj8uYMT4JzMlKoQw5rZzRjhPxxs7L32sFuMJvRwYW5WyDR5q7oyWYb/M5OtQZ4XLd2fGnZzGxIyyAmhpL7Tw/9+6H++w703XOw7+4D/fcc7v9kQ+B/wtoUqtJOADd40fL3Us62B37w1Jnz3TKc2D6Wh8TFntre9m9/s0S1NcFuVlnGhsWFr+/pGGvB+0slpr8n8sqBrv/zvpqJ6bq6Zn/tuSErMQROmDt52/ozms6u8KJKKd4T1duz2LyLAznDaZBj/xxfMH600W8uCVLji2dn+MNaR19irDNoApDQ958euGF+4Ti7dP/pAYtzYgq3rCi2/RYnjK56/38NJnYI0CnI5MLEJw6dgeThruiTxa67Zmb+i0Qtp+jwhNGT0Hs0HhRgUOIIp85QsOBXLEf1Fl9ipyHeM7kRBGSFZjukUpVd/zErFPdL+ObvakN9SaASEACJwBiVysFOdQUOnhrYsLTQ3vb82W1Vr++2Hl15Zf/Eifvzu9r1cAo85kuFpKRly5tnV/eltCs9jYSLWFRrylLnXRwXMJt+d7LF3z8YN5cznjL+4v0zD5wZfKKj1cQQS+lrB7s//5GF45wj7zs1aKlAh3BmKCvn2pwqE9Pbjg/+f1GtWaIeAMfF/8RAAuIQwDsij0W0xiV5/yvTHJNN1vrjr3RHn4toZ3UevVDGTxAiU6KabSojzoH4tr7Y1hHHfgKSTDOy1FWV3j/PUJZgWGZasO1I1x9ebwN6kY3MmBeBoPPHX7e/9t6tK4tzC9wWrQhU9sbh7q7B2AR0ncHFC7s7LNqjc7rxhkZV1q+WzE5EMHniEuUzn0v+yoEuSJoJuAsAhd68rOS2FSXAzdwCmR5t9PtC48pW6h2KNVnb+9X4oprMmhI7M9x1Hjrp+5eo3ipR75VWSwSoTDMDycMnfV/gwoS7UdLoPT742ZO+L/iTBw2RoERmxMmIkxEXBdnauEhAYsR14TqX/EeJpItwX3zr4f5PtYZ+juJ+/aMb/BsPHxdhA+AyG5kxKan05Nbmrl6bfUVK8lx3ri2zmDPD6NBA7JX9XRPQe4fODB5t8IFi/nEyaGZ+aP3Kc4mUdNXRUw5rJ/gF+RfCSlB178kBc1qp87Ji17wZmctm54JDMjGaMNo3ED/RNC4fgvr2oD+QtBZwv3F+vr0B97bwb4OpWom4Rw8FUO9gYmd7+JExj0ShE77P+RI7JeplxDlaapotEAKSRDyE0HPB77eHH0Jxv8755cv1uw72gHQh2m52DsqI35d8Zbf9SvqJ26tBYZbPPD6/ayJyZl471A0J3UqeTEpaPr+9tDCg6+yqr6OU0DvjWu/FL6gpugcjJ1uC5tYWmrF+Yb4iSXMqMqvLzKyfCEDSGGdVrF11fRYTewisnGNnhnvKGOyJPT8WZb/wKri6ok9qPDCWH24J/SyQPDqOMP04NB4YI87W8K/iRidcj6C4n18Cf+f3dWAwEGb2Ud/zPjvIc7vtV9L1iwpmV2ZarL2nsj0nBzr6o2ntPd0Qz+1os1hNgonbNpwZy3uoiaHwlY8yjcruun7/oOlz/LevKgEAlypvXGTSyk1i22v7xuNDcPjMEFiwajGEK0u5cWG+jffXnzycNHoJGeuWAyVywugeSuwdwxojOBDfxohrsl58QuSUMTAQex3F/brlgSdPtjeFgdDz+6jWesXJtp/qO2G3KaBDYX+6eQZolsLujAbSH5k50TRU1xK0FJNhRUX+5Qvbk6mxCIcR1s8CWDy0u+1It7nMEy4cmcqGxUXDf7pjdYm5p0KhtY1+yz4EwWjySIPPiruZzudWZNSUZNp4fwOpw8JkpwvBh5Kji3tYq08avZRMZloHISyiNcL1CIo7HG8e+vkfG4DK7wZkrAX9GIkPxX/zrP1l4e5cVw5OyaKqEXjlQHrF/bmd7SKWshaT2bCqMScranA6hpeQRi5UZTL7USld33W832wS5OxSb9WFMkZr5he4s1QTZ8ooSUa0Yw0WR/pznaHOgbiV3VSdL7U7wz2qNRCT+kuIFNc7x9DYoAADJjknneg8jOJ+ffKNh2pjAykgAHQc0/bzYRD5he2d4ajNlddXzc1dv6jA4raqKr15tLelJ5Km3hMCXtnfZSkmQ4gzeduGM7oxph6nRI5oZ5J6yMIgcq4z1NQdNbubeuvKInYhMDKj2Du3wmsubYnDG4ctht3rmv0WA+4UNi8vsvH+cpHUeNDsW0GAajzIRWq06ZALJv+0kaDUgeJ+HfL0jpbntrVd2Ecl4+0PmbYNxHbV2l9r7d6NFRYTIhkJ++Lbj6XLAfjgmcGj54asxGRS0sLZPfNm9qTGFJMBApIuBuJ6q4VGbq/t1czWD2Hk5mUXqyS5bWWxuVsgs53H+xMpKyWe957qtxKu58KRoayeZ2fAnQvN4HHzuUlEiNSFdPUr4pJrFJoz6o+lV9qF4ZTKUdyvN6IJ7VuPnIAUnLeRYePuDwpAxaOv21+e6a4NFRm5LotWM5SkrzbT87vaIWrJT8agm25oVBVdiDH+LtFFomvISmLD20dNjm2GyMxWl866JOdk4+IikMy4x0ukpTfa2Bk0r6f84GmftZhMVZHbbksZIUCkaX7tYEVZ6nKDxyZLAQRwRh15jo0o7tcbP3/h7Injg0DZFctxWMDJXt7V3tAWtLepVcWeO1aXQNLKNHDYyqqpy/7AYlIznt/dYcVBl1NXVnTj6sYxTtsBQJJ470Dmr583/cTGEtrRRpO+CCljzYK8snzPxX+3dFZOdq7DhM8MJUZEe+uoaW/O7sFYS2/U2vGlNQtyZYldQ+9gVcbfKyzX1KEnG5Vd58FS932ZyjIU9+uK9v7I/b8/dV7Rh90fbZmdMBIdSryQhpnyJ26tAmv1bhiJDsVf3mt/Mu+xhqGz7UErSR0pacXCtrJi/9XT2y9GVbTDx2e9sJ3rhrlV/JGGweZOk0c9Db5h0XuDG8W57hvn55pLiCT0rSOmA2Jn2vwha7WiCNy8tPjaeg098pwFOd+RqEfnYTFB7pKci5TOwwRIVcb/nZX5b9erxE1fcf/vx04MdEXPpz8y+3pCACjslf32e6lvWlZUXpZhOScyHTkzT29vA2sev0Lcuv6sRMc6DSYAus72H5vd1h/tGjC3it9T129uc1IAONi6xSMYS61bWACGOR+C2iZ/OGbO/nfn8T5Ima++xIUjU7lhwbVXfSnXsXF5/sMFrjsokXUR0XlY5xGdR3Qe5iJpac5FhuX7wnXOX03nEUPEAJhbnlnu/dTKgj/MzPwnQq5bf61pahy273T/Qy81mTYIG2tkRtpxamDX8f4NS+x0nsv2Kh9eX/6Tx+pANm/OpbIdtX317aE5FbYdBTQMse1wtxU/GZ3lFfmXLxhjejsAgCzrze15x05X8kT0ZKu/0kxYeU9dPzBzeTJVZd5Vc0e4d+sXF4CDmQhBS7S9O3rwzMAtK0rH/vlHG/zmGnwhJjN3Vtas0sxr8X30yHMX5/4kprcFk0cTRrfOIzBs9Zw6408eIMScwwwXqUxlcZa6got38tYoo06ZZqmswC1VO6VKSuTrXuWmo7hzIb70iyOpgAZMOh9tt3cBQ4HHUr947qy94g4A92yq+MmzZ0AI05NlShKh5FtHe2wU972n+k80+a3kyST1u9ZVlBe6hyLRMeYnybLx1t65WtgJPLyztveDayrG+FG9Q9G9pwfNug4sn5Xtdozw8q+Yk19V6mnpjI41EkUAksauE31jF/dYUjvTHrK2m7qsJpuQa3gt7pIqXVLlxX/TFX1qMLlLArPinshx3FiV8Q8wvZmOYZnHtzVv398DzLz749hxsLePdPsCNtfK2LCkcMmsHEhZisxQ8vR2O3cCtu7rgrh5PxkBoMB9N61SSJUQYzoQQIiIx5U9R2pAMoDRo40m6mAcqfcNDSbMuQ4I2Lxi5FRxt0NeuzDPnBUEo3tPmkiNPdMWaOmJgGQl4L5pWSEg5++hwE6YduIejKb+65ETkIR3q+ilI8tLot09sdf2d9t7VUbJXevLLfrMKOzAmcHmbntyZlI637K/00qeTMpYOjvn1pUFjM/hYkxfRFX0uvrS+uZCkA2Q2bFGf59/rGH3Vw92gW4mfs2FkiHfvOyK25K3ryo13+1DHf1j7fYj9T6ImR8yuVAylDXzsWgqMo3F/YfPnD5bNwhAADhIAtKXNkbJY6+32n7V+26pcmQ5rNTeYyTmT7y4x56cmYOnB+vO+S0F3Pknb68CAlnyagLyWKy1JMZ3HpgFSRmIAEZ8Q4mG9jFmmooDpwfNnZ7V+JKZWfMqs6707yvn5Moe2UT/MxIaSuw7NdbJ+666PktFaHl1kfsdswQEmXbiXt8R/NETpwEoEAEMrGSbjR2X9Or+zqNnfPZedV5l5i3LiywmvDPyx532+FZu3d8FSd1CRoeaoXzgxjIA8CqzZZo9avYbo3zQ79l1ZCbI+nDwAVL8xNgqlDZ1hU61Bs3FrzVjw6KCq6zm5lVmLarONJezxGGM9r+6YRytH7KW4X7D/FzlmspwR1Dc7eTrD9X6h9MfGbE/SeayruXJ1Mt77DcBvntDhcWEYIXtPdVfN+7azSmNP7+73cq0PanfuqJ4XmUmADikQgerGDXsrqr6ibOlPV05IF+I4RDYdaJvTLPgE32xQMpcwF0i6xddLbhBCN2wuMCcD4FED9cPGWOo5dQ5EG3ri1lJlSFw09IiQJDpKe6vHOx64pWW8+mPMrUyPzKLKj27q13TbT6a8SfryotKvFasZijRI9obh8abg7/zeN+Z5oClWthw76bKCyoJmcpKLkZbggjYtmcecHJxmOZYoz+pjb522VNn0qFF5/mFrnWLR9mWXLewwNx7I9NTrcFTLaOPqXXN/mgoZXpByQV1Sctm5QCCTENx1wz+rYePQ0Qfnp2BRCfiqyvsRGNwx9E+e69akO24eVmh1dp7NuTMbN3faSUmo/OiEu+H1pa98xeZ6mJCrjZCyJLR2Zt94FgVKPrFk+v2/ljnaBVIkpq+u860ze/i6qyCrFFqR6xdVJCd5zRl/2tEtN1jWG3sOt4HmvnjS7qoLnHPLs8EBJmG4v7brY1793UDY8AAZAoTE5wkAEnjwafrbb/wJ++oAZlaSfdS2NFG39lxWN/Ek8bWA91W8mSSxq0ri/Oz3rVX9cizGWRcxRRQVfQ9R2pCPi8wfrFWJsKp402jbGacagnUd0TMiTvnG5eMnk1YmudZXJ1lLmeJkD11o++pHjtn7fiSsXZ+nlOVAUGmm7h3+WLffOg4GOT8tF0mE2ci7WavHe5q6bbZTn3TksLq8gzQzU/eKUmFkn8cR2HV/acG6pvN58kIAJl+6o7qS/pGLndJM68UdicEUpq08+AsoJcFoAzYeXwUrTx0dkCYskYQAE7pjlUlY/nZtYvyzdr/7qrrD0avZo8VjiXrrR1fInATZrgj01PcH3jiZFdjECgFadzlOMyHQSLRxNtHbbaacTuluzdUWI3M0K37rCdEbtnXaSV0oBkLZ+W8Z9OPEMiQF10p7C7L+rm2/NrTZZfEZC706vGmwNU/8PVD3UDMuQ6U57vmzcgey8/esaoYVDNVyyXa0Rs72Xy1wkwnW/wdfebNILmQvfKaBSjuyPQT9yMNvp8/XQ+UARHno+0TDKMPvdIkhM1H5j5+axXzKFYS3hV2sN53sjlgLSbz4p4OSzaQxr0bK5TLfjHbsYaANGK2uyIZe49U61EHUHG5VjZ0hAKRKx4ADkWTB8+YrEGqGWsW5Ga4xuTbs3RWXlmxy8TknQAkjb0nB672oNb7rFRf0sWMQnd1MWa4I9NP3L/2m9rkYOK8QdjE7KO+B1XafbhvzzGbyzOtmJO7dn6+OQfadyIzweTzliIzh84ONnaETIs7F0qmes+mysv/xavMkWjO5dnulIpw1LFtzzyQjBHHy+6+2PEr1yI/1Rro7DdZg1TA+24Y6+nTTLe6YVGhubA7HcWY81ij38ppad24YX6uqkxTB0Bk+or787vbt7zdBhIDCiBRK5Yd44cAJI0nXmux/cL3bKqwWJtJZs/tbOfmZ/3PbG+DhPnD8Ulj3fz8xTUjRDycUpGDlV0edldkvaG5sKk9/9309vd0aco40uC7ckxm2PfGxPDjzFY2LDYR3Fi7MM/csklhtecC3YORK3w+P9Lgs7asvBkz3JHpJu7xlPGfv66FuHF+2j6R+6jvwUXfPNEbs3as9MrcvbHCm+O0ou8yPdHsN3uaKRzTXt7XCaqFWtjio5tnjDzwEciQV3B4b88wxt/YMxcSMpArfDtCdx2/YnLhjto+kwdT+aKqrFllJrIJNywuZF5zPgT+wfjuupHb3NQVqu8Im14SCQFOthQz3JHpJu4/fPp07dF+YAwYAZmaO6loLwo72x54ye5aSJVFns3WrAgoMSKpZ3aYi8zsOznQ0h4Cs2fcDZ6T57pr/RVrEGc7lhHBLg26iEDIdaC26rzlwBXGp+PNwVhyhEybtt7QobMm6+rpxpr5uaaiIgurc+ZXZprLmeGw+8TIYffac0OJsPnjS7qoKfFcxQkHQXG/Dmnti3zv9yeBECBw3mxg8rQdCIAhfvHUGdsv/PFbqiz+pkxfM1mb6aW9HeYcFi/EZG5ZWVySd8WTQR55JiVeuCjsrqrakbqKzo68kWMyFybCXQOx9r7ISELpjwTNuA4IAInesdqc3SOjdMWcHHMmMxLdfbKfixF+ZfeJPrCwrksZazDDHZlu4v6th0/4OiLnbWQUCpRMssOzyvbVDTa02lw4e/OKomJrVgQyO9I4dPjs4Bh/PBLXX97babo0hwBg9NN31FzlR1xypUuaycW7teg4J6/vXHCJ5cBIi49URDs6Utj97WM95kJVBs/Nda6YbbpA3R2ri836ENQ1Bc60Bi7/l+NNAUsbQmL1vDxAkOkj7jtP9D38YiNQBhRAZpMZkLlIjBJx7fldNkdm8rMcd64rsxaZ4ZHUlr1jnbzvPzXQ2h02HZPRjHk12betLL5qQyBDXvhO2F2SjJ7+zMMnK0ZIb78synG5g5jB+VtHe80G3BfPzCzMcZntwhvnF3iyVFM+BHpUP3TZgOoPJ+o7wqZ3UwWAS1ozPx9VDJlG4v6th44bgRRQksZyHFYm7/TJna26YbOP2Ic3VIDMrKxLZLplf+cY8++f2d5mpWpzynj/mlJ1NK+CDGUBiPOXVmV939Ga8JDnEsuBK0Rm6prfuxJq6Qmf6zLtOvC+1SUWer6yyDunIsPssumNw92XTduHevvNm0HqvKbYs6g6GxBkmoj7b7Y2vrG9AyQJGIBCpsS0/UJk5mhd3yt2b6tuXlG8cFaOlYR3hR2q9x2pH91xPp403jraYyG9nXmUT95WPeoPZqhLJJIFwAkBzWBv7Z1zxSSZi5FoQ2doMHhJVabdJ3rjQTM7kwJAZesWWjvhSW5bWWTWh2DvycFoInXx3x2t90HSsHDod9W8XAy4I9NF3Iciyf9++Dho4sKpJQJkKrVPEy9ut9nhXZHox2+pshJ2JwTi2itj2FbdUdvX2Bo07SeTMlbMzl02e/REPbdc4ZQqudBkSW9qza9rKBk9JgMAjA4MxI82XHKUadvhHrM2vxXF7kU1Fue/GxcXgWRmO0cirX3R9xi3HTs3ZKX6EsAtyzHDHZk24v6dh5qaa0MgU5AAZJLeWksWcEqvHenzBZP2XvVDa8skt2zFikBiz+1sN0b7xWd2tFrJkzH4n902pmQeSkiGvIwLTVGM3YdnpkLOESwHRhicAFK8tvFdcdd042ij39wKI2WsX5Sf4VKt9fySWTnZOWaqHhICMX3bkXcjMyndONIwZD7DHcDBls7EDHdkeoh7zGiuH3ycgAQhNzAGMplyTZRpR3f4yTfb7L3q4prsm5cVWfERU2htg+/qnifBSOo1Cx6/hnBnO4cr6o2FLMdySmkkpuw4OOtqGZCXDQtHLzIhON7kO9Nu0h2BwvtMJkFeTEmu+8YFeeZiYpS+fbT3nT81dYbOdUZM76bqvKrYjRnuyHQR987oz/7l//78299/aPHqZoi7Ieo8P7+bUjDxyJYG269698YK4JYiMwl9y96Oq/zI7rr+zl7z6pPU37e6dGbpWA2tvPJsl+qoO1tSf67YhLjL7PBZX/zCUaYdtb0QNeOOYAhXprruqnX1RmXz8kJzPa+w/Wd8nQPh4T+dbgtoUc30ElMzblyQ53YqgCDXvbj7ErvaAi+mku5bbjr9ox88+vl/3FqYHwS/5/wR9qkj8So7XO87dHrQ3qveua48p8ADFlJxFLb1QNdVagE+/XabFY9fgA9vKB/7D7vkCrdc9cbuGkhJY9pNPT9SktbeaFPXeaF8/VCPuZwTnc8p91SNz1Jx8/IS4jbnQxAcSh4/d9774XD9oJV4Ghcr5+SifiHXv7hzobWEfmpA1OBsaChbYfSv7tvzi+8/9Kcf2+2WdfB7QKcmJCOtEMLj+ou7Ouy9amm+a7O12nsyqzvnP3iFwSYS03Yc7zMdk9F4eXnGxRX1xhBfoanI2n21pWPaSr3o14yYfqrVDwCDwXhtY8BcTEbnt64sJmRcb8GcisyqQrfJY1Pi7WPnLf53neg3nQQ5nOG+ADPckWkg7t3RZ32JPZTIBBijcjLp8PndpQXBL31uy4/vf3T9plM0rkDQBQKmhMSr0hNvtQciKXuv+me3V1vZQCYASePlfSPnzGw70tPaHjS93ZfSP7KpMstjLmjQ3nzbYHfuyB6/VxvYYf/pAQA40TTU74ubE0qJ3DRuS0WXKq9blGfO/lei2470AoAvFK/vMH80TOdVJe4lNThzR653cU/xodbQLwEEAKFEpkQaDrqGoo5A0LV8fscDX33qG195etacbgh4IKYCmWyJV+m5Jt8b+7rtveqtK0tmzjBZ2/P85J2+sLstNZJNyqsHuky7TgoARRrRvf3qzC6rkC2UH2F036lBGC69ZCpb3BDZOaotCSe3rCg29wsSbeyM9PtjJ5v9g4Nx00cxNL5oRpbLgRnuyPUu7m2hX0X0s5RIFGRKlHe+FCVCCDIUdOsau/PW47944OG/+/tX8nIiMOSGlDzJ+k7hD28223tJj1P68IZyS5EZeqY9dKzxvaeZApGUlVrYKX3l/LwbF5oOGsytzJxTbvrAJ8i0sTPc748eOmvSDz1l3LggvyTPM/6eXzU3X/aYC7vHgsn9p/tPtQStnPsVfO0ijMkg17u4h1InOyKPUiIBMEJkgPcqESUipUmDQ16Xqv/dJ3f86gcP3Xn3AUUQCLiBk0mTeAd7u7bX9sLZ92yqBJcMZkv6EQIx7cm3Wt/z13vr+js6zddd0vg9GyqY+QARo3TZzGxzPosAwMhQMPXcztaGzrC5Y1YGX2+TRM6rzFxUnWmu5QJe2d91pHHQdCRNADilm5cVA4Jc3+LeEvppivsJUEIkSkaux0GIIETE4srAkKe80P+f//zSD/77d6vXNJCoA8JOgMk4xcpowJd46s1We69644L81XNyLSW8sy37ut5TTuT53R2mYzJcOLMdd2+ssNb+DUsKrNwLQn72x7O9Q0lzNr8Otn6xPXWlCaEbFhWYW3Mo0muHu9880gOqyQp5Bi/Odc4uz0DxQq5ncR+IvdEXe5USGUCiRCFX/TqECAAIRZzhiLpuRfOPvvn4l//tuarKAfC7IK5MQrqkzH738rmEBSEedfJupTYTa2gN7LvoNJM/nHplf5fpmExS37ioYG5lprXGr1tYYC6+cWF1Vtcc1U39ls5ryr0r59jml7tucYG5l0kiLd3xtt6EhYD7kprMLI8DxQu5bsXdEPGm0E84JAEoBXmM9o+ECIPToYDbMOiffuDog997+DOf2ZbjScCQF1JsQqM0Kj3V4t97wubC2R+5qdKV7TCt7wRA4y/ueTdB8/VD3Z1dIQvH4j9+a7Xlxs8pz5xb6TUdmQEwL5HGytk5NrpurV9UmJNvsuQhtWSPwbnp/VsExf3aoivyRCB1mBKFwHBAxsR3IURomjTod2d5E5/7y7d+8t2Hb7vjmKwzCE5wIJ6/tM/mhPeaUu/6hfmWTCLpln1dscT5yMyrB7rArMbqvLjU+0Ez6e3vlWjG1szLs2KCZp7NK+x03SrOdS+ZmWWl202OneBgN2KGO3Idi3tC72oN/ZIAJcAYKAQkCxchBKIx2ed3z53R/+0vPXv/tx5fvKQVwk6IOgDEREi8Ij29o30gkLD3qp+6o8b0nioASKypM3TwzCAADAYSrx3sNl0LO6nfta48L1MdT+NXz8uDdBfN4kL1ypuW2jz/ff8NpVYcIEwOn5XFnqUzMcN9LEtRFPdrk+bQT6J6MyEyBYkQ2Zpj6rC+A0Aw4ozGlc1rGn727d998V9eLCnyw5AXkkra9V2mXS3BZ99otfeqH7yxrLw8w3RwgwAk9afebgWAXSf6e3pM+skIAIXdaz69/T3cuCBfzlCsnMgfOym+bHb2nPIse6+6dkEBqCy9A5POF1dnoaXM6A+jMLATrklxDyQP90Sfp1QlwAiRCWHjvCAhwjCoL+CmRHz6nv0PPvDIR+/b7Rr2LTBoeicBEv2j3Q7v2V7l1hXFVkIEMnv7WC8AvLy307S8poxFM3NuWjbe/JM5FVmzSr3pjczoxvrxmYWNyMLq7PIiV3pbzvma+dNo2m7Vs5sI0FDcr0VxF02hH2siSIBRIlMi27UEI0QkkvKgz1uUF/qPz2356f2PbLq5jiVlCDlBpC0Q72B7z/nq20P2XvWTd9SAYn4WKbOz7aEn3mzZebzPdExGN+5cXy6x8T5REmNLZ2alVyIlsmGx/eKe6VbXLyqwckJ47Gsjh3Tz8hKYNhCrAsVFEqY9156498ZeGoi/RYlKgFGQAaiNIVpCBBARiTr8QdeSuV3f+9pT3/rKU7Nnd0PAfcG3wO7vw0gkEH/0tSabQwQL8+fMyDQtNAQA4Gu/qW0fiJmLyXAhe9WP31JlS+M3LilMY9hd54VFbqt19UZh3cICK7sdY8TgRXnOuRWZMH0gViogE0J0HkVxv8bEXeOBpuBPAAQAo0QhRErLE0WEECQQcsUT8gduPvnz7z76D/+wtSA7AkMeSEr2T+Fl9tiWcyFbfcQcCvvoTTOs5BRS0tARSpmdOKeMG+blLajKskciFxUyd9rC7hpfXJ2Vm+lMx7XXLy5gHjl9LV9YlZHtnUYZ7hSs5aoSnQdR3K8xcW8L/yacOsHO76Mqad0TJ0RoGvP53S419Tef2Png9x+69yN73VRcCMTb9wLLtK0rsuNon73tv3tDOXFJltJmzG8zGOL/3FFjV8trSryVhS4rR7HGtMjgm5YWpOmZWVidM78yI10xJc5vm2YZ7ow4LL3jVBNhFPdrSdwjWkN75GFKFQISMZnYblXfgRCIJ2Sf31NWFPjKP778o/sfWbf+DI2pEHIB2OguKZ58y+bae8vn5G5cbMnh3Xygo6jEe6eZ0hxXx6nKq+bmpCV4LQS4pDtWl6WpJxilq+bkWlkwjd5yAJmtmju9MtwZdRHzcVcCTDOGDJGA6c21JO4toZ+ljAECElzZRiZNEg8AkagjEHKuWNj+/W888bUvPVtd0wd+D8RVe/TdwV490tXWa7OP2IfXl0/EgaCksXl5UUGWneGCzcuK0hK81kVlgSutYevbVhWn5a0yRG6uOn/GdAq4A0jUS4li/oVlCaM3preiuF8b+BK7eqIvUKJeOI860YcUhgPx/qBbS7G733fsl9976DN//XqWNw5DXtDG7VsgUV93+MnXbTYBvnN9uTc3bfGNd2aUEv3EbVX2XnXNgvy0BK81Y+3CfE8688RvXFjozVbt73PNWDUntzDHPa3kSaE5jLiEyaPSBKjOw4Pxt1DcrwG4SJwLfo9DAoARkCnIk3UCjRCR0tjgkMfjTH3+L9/65fd/+8E7D8oGvWAgPJ4Hmb1gd+296hLv5uVFkNTT2CM6ryj1blhsc/LJvMpsK97uYxiL7lid3lTCykLvqrk59vsQGHztojyYZkjUK1GPEKYfA0Yc3dGn43onivtUpyvy1FBiPwWVgp2J7ZajNMMGwoNDnpkVvm9+8fn7v/X4kmUtEHFAVB2eOVgT9wNnhw7aXTj7vs0z0tsdKeNDa8sy3DZXBZIltrja7mx3Q7izHbaPQ5ezYVGhlUrlV18eOdnmZdMow/2CRrtkmgWmTY6AEClh9J3xfylp9F2hR/W43ulL7OyKPt0W/m17+BF/8qCA6+pcqzT1m5g0BlpCPyUgCBBqx3lUu6bwABAIOSVm3HxD46pFbS9tW/LYU+s6W/PBlQJHCoRJjafEiGu/3dq0er6dE7TNK4oLij39vri5vPWxi45MPzJuy4ERuXl54RNbbY1TacbSmrzqkrSHrTctKwQHA2HfJMTgxfmu+TOypt/skygs35rmMuLyJw8f6v9YnmNTprJEZYUCRJL3x7W2uN4e09sTRrfGgwI4CA4AlChZ6qrZWf/PI89BcZ8gWkI/i2iNjLoIkYilQw1plfhhA2FV0f/swwc2rml4/Lk1L7y0MjLkhYwYSIY5iVfZ87vavvFXSwqybducLMh23Lm27NfPnAEpDYHmlLFods66xWnJLNywuEj2ypomLB5Bvxydb1gyEdkmN8wvqCn3NrVHTBsmX3FY4ktnZU2rDPd3cEqlQnBLL71gxKlxf2fkiS7y5HCNNgEGCE4IG3YuYcR50VJbDCX2Hhv4yyV5v8hQFmJYJu2EU6c6I49TqhAiD59KnYqzCyKSGvP5PflZ0S9+9tUf3//oxptOsrgCIRcIM+mSEu3rCm/da3Pk/Z6NlSCnx9BK53+6eYYipeUpqinxVhe5bYvMCAAn+9CNZRPwPLhUefUcW8PunN+xatrFZIbxygsIsT66E5Ak6mHEzYiDEYdE3BL1MuKiRL0sl5pI1JPiQ2f8XzFEDMU97TQEvpPiA4TIlCiTHm2/6jMEABCJqYGQa+nczge+9uQ3vvrU7FndEHSbS5ek5MXdNu8C3bqyeNGsdGzxCVeWet/mqjR1qSJLG5cW2JYzrvPKIveSiTLL/dC6ctveLQHgkNbMn6Ye7l55vkS8AvjEfBwjrnDqVG/0JRT39NIX3zKQeIMSJwWFEsVeG5k0TeGFIIGwM5mUP7S57sEHHvmrv9rmdSZhyAv62NIlHdKrh3tOtdh5eFqW6N0bK+w/E5TSb1paNLPMm77+vGWZfTnjKeOmpQWeiTLL3bi4KCPHYU9CpMZnlnsW1+RMT3F3SuUqKxZCn8C3mPqSu1Hc04ghYk2BH3GhU5AoUa26TEyKxIOmU5/f41S1z/3FW7/8wW9vf/8RSWMQco3uLslIPBB/cpvNCe9//v6ZWfkum1M4gHzmQ7PS2pMr5uY57fJ2p/An6yom7BkoK/CsnJNtz2pJMzYvK7KxIqDJh1liRBGmJ1Vi+EiKDfeNKHnODRPp8khASug910HmzNQV9/bww4HkYUYclChkCgdkrqTvhIh4Qh4KuGfPGPjOl5777jcfX7T4nTJPV/02CnvqzZaErVGUqmLPn91aDXH7pj9Jfen8vA+kOYQ9szRz7YI8GxwUdJFX4Fy7qGAin4F7N1baU5iJwublRZP1JDPikKjHbDKiAC5RLyX2DEhFrrtkmjnhaivgGmeKintcb28N/YIQmZyftkvXYueeL/MUdoYj6uYb6x/8zu+++M8vFhUER3GXlFl9e+itIz32Nubv753rzXbadnJSF/9w7zxVTvvz86G1ZTYsOJL67SuLiyf2eOcHbyzLyBt3h+u8qMi9ecUk7qYSp1QhwOy0wFCZbQOSR55d4Lrd4BO0ySnAcEiF5NrUnGtA3FtCD8aMdkrU9Pn6TqDEC4NTX8BDAD519/4HH3j4ox/b7aL8iu6SBEDnf9jWam8z5lVm/sUHZ0Lcjgo1cX3l4oJP31E9Ab1398bK7AIn6GI8LytI5L5bZ0zwfa8sylg3f9zLjqRx26qi/PQYFI+RTGW5MOnzI4TIVBbb2IYZ3r9VWSEXqQn4vkIYWcoquPaZiuI+lNjbGXmMEQcFmU3V9EcLEp9ISoNDntKC4Jc/v+VH9/9u7fozLK5A2DlClMYp/XF7e6Pd5Zm+9OnF5RWZ4w0EcwEy/ebfLJeliXh4Kgu9n7q9ChLjGJNSxtyazFtXlE78Tf/0+2vGZTokAGTy6ffNnNxHN0tdIVHP2PNVBHCZZeY41tvYBqdUNjvr3wUY6Q7OcJFySuVF7jtR3NOxJuJNwR/qIkJBYdf+tP09URpCIBx1BIKuFQvaf/CNP3zty89UVQ7AkAcS8iUqQEk0EH/jYLe9DSjMdnz/c6tAwLi2KCOpf/zYgvetnrhAwd/eM9czHisuzfjCfQsmZUPynk0zli/Itb7VEdPet6ZkUoal90RFch3rxx4VMXgsV13nkmw+t1zo+sDsrH/nImE+RjT2ObsuwJiV9UWFXg+5SezrX//6lGpQT/SPzaEfM+KkxMGIkxAK1xeEAABJJBUCsHhe100bTjNVa24qTvk9oHBg/Pw0Xoiozv/8/TbP2ubPyNKE2LWvC2QKFs6GhFO3bKr8zb+tldnE3Ze8TGc0mdq1twtUyUKD77ip7IG/XzWegzDW3y5Ka8q9j73eLIT5Us86d7mkh7+yvjRv8p0gnVJJX3yrAGPUIgpCaIy65uX8l8rsT8zPVJY4WOFQYo8h4nbt1r47JokYIcqc7K8Uu+66PqRmaom7xoN1vv+rcR8lDkad9FpLkjEl8Qan8bjqdac2rmlYvaopGFPam4pEQgHFAApASedg/H2rS0rzXfZ+9C3Li8OG2HeoB6gZxeECItotmyqe/sYmr3OiZ8FrFxZsP9XX0RIExUyMLqbPnZ359LduynJP2sH96uIMt5u+sasDJDOjKReQ1L//z6vv3jBjKjyuKiuSWeZAbBsh7Cr6LoTGwZiX85+5jg1paolXWZDtuCGqNcX0NgBBCBunRAjgXMQFaNnqDQtzvpvvvPm6EZmpJe4toZ92R59h1M2Ik1EHgett2n6Jvg9nxGtSMqGUFgZvu+n0jKq+9u4cX2ceAAGFi0iKUvYn68tt/+g7VpUUFXm2H+3RQsnRRUcAJHQA+IdPLnr039d5HJMQKJMZvf2Gkjdqe/s7I6CwMb3OEa2q2rvlu7dWF09ygYu1Cwupg2zf2w1AgI2h6bqAhPblzy79f59YMnUe1wxloUQ9Q4k9HFL0MosnAdzgUUYdc7O/XuK+O60tcbCiItddTqk0YXQnjQFDxAQIQoCMtYC9EGAIoXFIcpGSqDtbvWFW1r/WZH7ewQqvK4URYqqkc0a0+v29f6LzEKMuRtwWKrBcuwhBJMnI8saHQs4nX1r15HNrhvqyQIlVlTuOP3qX15WWmXJ9R+g/flP74q72VDAJjIBEgV30enABOgeDE4e0ZmnhN/58ya2TXcCzzx/7y/v3bH2zHSgFBxt5TBIASR10vmldySNfWl9Z4J0it/jXW+r/6SeHI4MJcEpXdOg0BMQ0NVP+zt+t+Py9C6bgg+pPHmgJPRhMHTN47MKjIgBAppnZjjVV3r/1KvMn8K1JBbWTg/G3Qsm6uNGZMgY5pITgMIIhpwAAIJQAoaDILFtlRW5pRpa6Mltd45TKrktVmULiXjvw2e7oE4xmStTFiPN6DchcZYIsgDhVzeNO1rcWPPrU2ldeX6IF6TM/XXvvTeXp+9gzbcEX93buOzVwujXQ60/oBgcASkm2R5ldnrFqTu6Hbixbt7CATI27IYT4w1vNP3nu7IFTgyJuABfvSrwAoAAOtmJuzt99ePanbp81kRsDY6GxK/CDp08/+Xb7UH8cDH7JEy4ESDQjx3HX+tIvfGzB4urcqfykhlKngqnapNEnhEaJwyGVZKsrXVL1JLZJ48G43hY3ulKGX+dBQ8TecSwghFGiMuKSqFem2SordEplCs2C6zowMIXEfSD+5uH+TxJCGHFJ1H0dnCCwPIUnRHjcCcaMvUerf/yrtaXedW/9fCLigEnNGAql4klDACgSzfYqHucUvQsG56da/cfP+Ru7Qj2+uG4QSnlRjmNmacaSmpxF1dkym7rps/2B2NGGodNtgdbecDjGAcDjpDOKvHPKM1bMzi3OdQOCXDfiLoR+oO/uoeReiXgl6qLEOc3uwvAtEFwYAnQBHASTGcvK0IKRzNfe3PS3t325rCADH1YEQa4xcW8L/+bk0Bcl4mbExaiLXO/LJQAhhM7BABjeBQIBgoKssDynVO6UylSaJ7M8heZnOUoVUsQhixGGDyuCIGNn8tfdSaO3OfhjChIBmRLlulN2IYBf2OSB4W16SlSZ5jikYpUVKjRXptkeeZZbrlJZoUQ85LIEXtR1BEGuPXFvDv1vVG+TaSajqu0HEyYcLkCcn5gLDQAoUSTqlVimTDJllumWatxSjVMuV1mRTDMZcVCiTrutYwRBrntxD6aOdYR/L1EXJTIlyrUmc2JYyrlICaETIjHilIiTUadMs1VWnCHPd8s1CsuXSYZEMyTqnVb5nQiCTF9xbwr8UBMBmWRNeYOw4VGHc6G/Y11EgFIiU+L0yHPccrVLqnKwEpXmqazAIZWhjiMIMk3FvS+2tS++VSKeqVqOQwjgIIQAzkVCAJeIR2X5DlasskKZ5TpZcYay0ClVyCznGioUhSAIinsa0Xm4MXg/gKBEmTLTdgEAXOgCNAEGBWU4lqKwHJc0wy3PylAWOFmpRDMpdaCaIwiC4j4CbeFfBZNHZZpNQYXJ8/UVYAhhDG+EEqCEMJXlueVZHnmWU6p0slKVFaqsQKJefFYQBEFxH4WY3toa/hUjTkJkMtHpj8O5iToXSSBUplmqVOigBSorciszM5UlTqlCodlw/efaIwiC4m43TcEfJPQumeZQotL0T9sFcCE0AZwAJUSiRFVYSaayOFNZ5lZmqbRAppkS9WBKIoIgKO7W8Sf2d0WflqiXUYWdT3+0/ZSsuHCaPyUEZ9TlkMrc0gynVOWRqzKUZS6pghIVbz+CICjuNomu0BuC/8NFQqJZhKgA1CZlJxdsmg0ADkAIkRSW45FnZ6s3ZMjzHVKpzHIlgq5MCIKguKeBzsgTg/HtEs1gRLUr4USAYfA4AJdYlkuqckmVTqkiQ5mfpa6+PmohIgiCTGlx17i/KfRDShRK1PGdR31nkg4CDIl6sxzLchxrvMoSl1Sh0FwMuSAIguI+cTSHfhrVGmWWzYhq3rGdnD/oDykCskQzHFKRR5mdq27MUBY5pTLcDkUQBJkEcQ+nzrSHf8uI68J5VBPzdC5ShogTIrmlqkxlqVdZ5JFnuqQZCssnmLOIIAgyieLeEPjvpOFTaS6FUc+jDu+O6lwYBAglzCmVZSrL85w3Z6nLFJqLk3QEQZApIe79sdf64q/KNIOMltj+zu6oygrcymyvMs8rz81W16isAO8WgiDIFBJ3LpLngt8VwClRGVVGOvwpBAguklykZOrNUddkO9dlKyvccrVMs3CejiAIMhXFvT38iD95UKY5FwzC3j21JMAQIgUgKHFlKktyHRvynZtdUs21X7UDQRDkuhb3hNHdEvopJS5KFArnfX0FCC4SAEKiGS55doa8IMexLtexAVMYEQRBrg1xPxd8IKa3yDSfEhUIFULTRYQSNUNZmKtuyFSXeeRZKivE2AuCIIiNECFE+q7uTx062PthAEKJkxKVAFDiyFJXlHnuy1ZXMzQDQBAEuRZn7o2B+5NGv8ryFZbrlmZmqctL3Pe4pBnY7wiCINequHdGft8bfT7bsabAeVuuY2OGskihudjjCIIgE0C6wjKGiJ0e+pJXnlfgusMplZHJrsSNIAiC4m4DuogIwWWagV2MIAhy/Yg7giAIMomg6xaCIAiKO4IgCILijiAIgqC4IwiCICjuCIIgCIo7giAIijuCIAiC4o4gCIKguCMIgiAo7giCIAiKO4IgCIo7dgGCIAiKO4IgCILijiAIgqC4IwiCICjuCIIgCIo7giAIijuCIAiC4o4gCIKguCMIgiAo7giCIAiKO4IgCIo7giAIguKOIAiCoLgjCIIgKO4IgiAIijuCIAiC4o4gCILijiAIgqC4IwiCICjuCIIgCIo7giAIguKOIAiC4o4gCIKguCMIgiAo7giCIAiKO4IgCILijiAIgqC4IwiCoLgjCIIgKO4IgiAIijuCIAiC4o4gCIKguCMIgqC4IwiCICjuCIIgCIo7giAIguKOIAiCoLgjCIIgKO4IgiAo7giCIAiKO4IgCILijiAIgqC4IwiCICjuCIIgKO4IgiAIijuCIAgyxfn/BwBcPl/tlLTHbwAAAABJRU5ErkJggg=="/>
		</div>
		</html>

		';

		exit;
	}

	/*
	 * http://sisowdummy.localhost/StatusRequest?shopid=0&merchantid=2537802310&trxid=DUMMY_0028666914&sha1=01f94250b35fc47de215978bbb342a74d8e9959e
	 *
	 */
	public static function statusRequest() {
		// sha1 key : trxid/shopid/merchantid/merchantkey
		Logger::log ()->debug ( 'called statusRequest' );

		$params = ( object ) $_GET;
		Application::getInstance ()->store->write ( 'status_params', $params );

		$trxid = $params->trxid;
		$status = Application::getInstance()->config->read('transaction_status');
		$amount = Application::getInstance ()->store->read ( 'transaction_params' )->amount;
		$purchaseid = Application::getInstance ()->store->read ( 'transaction_params' )->purchaseid;
		$entrancecode = Application::getInstance ()->store->read ( 'transaction_params' )->entrancecode;
		$consumeraccount = 'NL00TEST1234567890';
		$merchantid = Application::getInstance ()->store->read ( 'transaction_params' )->merchantid;
		$merchantkey = Application::getInstance ()->activeMerchant;

		// sha1>SHA1 trxid + status + amount + purchaseid + entrancecode + consumeraccount + merchantid + merchantkey
		$sha1 = sha1 ( $trxid . $status . $amount . $purchaseid . $entrancecode . $consumeraccount . $merchantid . $merchantkey );

		$data = ( object ) [
				'transaction' => [
						'trxid' => $trxid,
						'status' => $status,//provided by SISOW
						'amount' => $amount,
						'purchaseid' => $purchaseid,
						'description' => Application::getInstance ()->store->read ( 'transaction_params' )->description,
						'entrancecode' => $entrancecode,
						'timestamp' => DateTimeUtil::getNowGMT (),//provided by SISOW
						'consumername' => 'TestConsName',//provided by SISOW
						'consumeraccount' => $consumeraccount,//provided by SISOW
						'consumercity' => 'TestConsCity',//provided by SISOW
						'consumeriban' => $consumeraccount,//provided by SISOW
						'consumerbic' => 'VWITNL02',//provided by SISOW
						'issuerid' => 'VWIT Bank'//provided by SISOW
				]
				,
				'signature' => [
						'sha1' => $sha1
				]
		];

		$parser = new ObjectToXML ( $data );

		Logger::log ( self::TAG )->dump ( $parser->__toString () );

		self::htmlResponse($parser->__toString ());
	}

	private static function jsonResponse($data){
	    header('Content-Type: application/json');
	    echo json_encode($data);
	    exit;
	}

	private static function htmlResponse($data){
	    header ( "Content-type: text/xml; charset=utf-8" );
    	print $data;
    	exit;
	}
}

?>