<?php namespace Utils;


/**
 * Class used to persist a temporary array
 * @author Aaldert
 *
 */
class Store{
	
	private $file;
	
	public function __construct($file){
		$this->file = DOC_ROOT.$file;
	}
	
	
	public function write($key,$value){
		
		$ser = file_get_contents($this->file);
		$arr = unserialize($ser);
		
		$arr[$key] = $value;
		
		$ser = serialize($arr);
	
		$storage = fopen($this->file, "w");
		fputs($storage, $ser);
		fclose($storage);
	}
	
	public function read($key){
		
		$ser = file_get_contents($this->file);
		$arr = unserialize($ser);
		
		return $arr[$key];
	}
	
	public function all(){
		$ser = file_get_contents($this->file);
		$arr = unserialize($ser);
		return $arr;
	}
	
}