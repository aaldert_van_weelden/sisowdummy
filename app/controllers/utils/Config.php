<?php namespace Utils;


/**
 * Class used to persist a temporary array
 * @author Aaldert
 *
 */
class Config{
	
	private $file;
	
	public function __construct($file){
		$this->file = DOC_ROOT.$file;
	}
	
	
	public function write($key,$value){
		
		$ser = file_get_contents($this->file);
		$arr = unserialize($ser);
		
		$arr[$key] = $value;
		
		$ser = serialize($arr);
	
		$storage = fopen($this->file, "w");
		fputs($storage, $ser);
		fclose($storage);
	}
	
	public function read($key){
		
		$ser = file_get_contents($this->file);
		$arr = unserialize($ser);
		
		return $arr[$key];
	}
	
	public function readAll(){
		$ser = file_get_contents($this->file);
		$arr = unserialize($ser);
		return $arr;
	}
	
	/**
	 * Write a JSON array configuration string
	 * @param string $data  The JSON config string
	 * @throws \NullArgumentException
	 */
	public function writeAll($data){
	       
	    $obj = json_decode($data);

	    if(is_null($obj)) throw new \NullArgumentException("Failed to decode config JSON");
	    
	    $arr = (array) $obj;
	    
	    $ser = serialize($arr);
	    
	    $storage = fopen($this->file, "w");
	    fputs($storage, $ser);
	    fclose($storage);
	}
	
}