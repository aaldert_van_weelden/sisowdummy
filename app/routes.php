<?php namespace App;

use PHPRouter\RouteCollection;
use PHPRouter\Router;
use PHPRouter\Route;

class Routes{
	
	private $collection;
	private $router;
	private $currentRoute;
	
	
	public function __construct(){
		$this->collection = new RouteCollection();
	}
	
	/**
	 * Add the routes for this project here
	 */
	public function add(){
		
		$this->collection->attachRoute(new Route('/', array(
				'_controller' => 'App\Controller\IndexController::index',
				'methods' => 'GET'
		)));
		
		$this->collection->attachRoute(new Route('/merchant', array(
		    '_controller' => 'App\Controller\IndexController::setActiveMerchant',
		    'methods' => 'POST'
		)));
		
		$this->collection->attachRoute(new Route('/merchant', array(
		    '_controller' => 'App\Controller\IndexController::getActiveMerchant',
		    'methods' => 'GET'
		)));
		
		$this->collection->attachRoute(new Route('/config', array(
		    '_controller' => 'App\Controller\IndexController::setConfig',
		    'methods' => 'POST'
		)));
		
		$this->collection->attachRoute(new Route('/config', array(
		    '_controller' => 'App\Controller\IndexController::getConfig',
		    'methods' => 'GET'
		)));
		
		$this->collection->attachRoute(new Route('/status', array(
		    '_controller' => 'App\Controller\IndexController::setTransactionStatus',
		    'methods' => 'POST'
		)));
		
		$this->collection->attachRoute(new Route('/status', array(
		    '_controller' => 'App\Controller\IndexController::getTransactionStatus',
		    'methods' => 'GET'
		)));
		
		$this->collection->attachRoute(new Route('/health-check', array(
		    '_controller' => 'App\Controller\IndexController::healthCheck',
		    'methods' => 'GET'
		)));
		
		$this->collection->attachRoute(new Route('/DirectoryRequest/', array(
				'_controller' => 'App\Controller\IndexController::getAllIssuers',
				'methods' => 'GET'
		)));
		
		$this->collection->attachRoute(new Route('/TransactionRequest/', array(
				'_controller' => 'App\Controller\IndexController::handleTransactionRequest',
				'methods' => 'GET'
		)));
		
		$this->collection->attachRoute(new Route('/confirm/', array(
				'_controller' => 'App\Controller\IndexController::confirm',
				'methods' => 'GET'
		)));
		
		$this->collection->attachRoute(new Route('/authorize/', array(
				'_controller' => 'App\Controller\IndexController::callback',
				'methods' => 'GET'
		)));
		
		$this->collection->attachRoute(new Route('/statusRequest/', array(
				'_controller' => 'App\Controller\IndexController::statusRequest',
				'methods' => 'GET'
		)));
		
		
	}
	
	public function config(){
		$this->router = new Router($this->collection);
		$this->router->setBasePath('');
		$this->currentRoute = $this->router->matchCurrentRequest();

	}
	
	public function getCurrentRoute(){
		return $this->currentRoute;
	}
}








?>